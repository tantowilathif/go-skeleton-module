package rpc

import (
	"go-skeleton-module/rpc/qoingrpc"
	"go-skeleton-module/service"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	grpc "google.golang.org/grpc"
)

var RoutesServer *grpc.Server

func init() {
	qoinhelper.LoggerInfo("Initialization rpc")
}

func RouteGrpcServer(s *grpc.Server) {
	qoingrpc.RegisterCustomerServiceServer(s, &service.CustomerServer{})
	qoingrpc.RegisterUserServiceServer(s, &service.UserServer{})
	qoingrpc.RegisterExampleServiceServer(s, &service.ExampleServer{})
	qoingrpc.RegisterAuthServiceServer(s, &service.AuthServer{})
	qoingrpc.RegisterBarangServiceServer(s, &service.BarangServer{})
	qoingrpc.RegisterBantuanServiceServer(s, &service.BantuanServer{})
	qoingrpc.RegisterSupplierServiceServer(s, &service.SupplierServer{})
	qoingrpc.RegisterJabatanServiceServer(s, &service.JabatanServer{})
	qoingrpc.RegisterPembelianServiceServer(s, &service.PembelianServer{})
	qoingrpc.RegisterPenjualanServiceServer(s, &service.PenjualanServer{})
	qoingrpc.RegisterKartuStokServiceServer(s, &service.KartuStokServer{})
	qoingrpc.RegisterSalesServiceServer(s, &service.SalesServer{})
	qoingrpc.RegisterPemasukanServiceServer(s, &service.PemasukanServer{})
	qoingrpc.RegisterPengeluaranServiceServer(s, &service.PengeluaranServer{})
	qoingrpc.RegisterKategoriServiceServer(s, &service.KategoriServer{})
	qoingrpc.RegisterTransaksiServiceServer(s, &service.TransaksiServer{})
	qoingrpc.RegisterDashboardServiceServer(s, &service.DashboardServer{})
	qoingrpc.RegisterLapTransaksiServiceServer(s, &service.LapTransaksiServer{})
	qoingrpc.RegisterTesalexServiceServer(s, &service.TesalexServer{})
	qoingrpc.RegisterTesRasyidServiceServer(s, &service.TesRasyidServer{})
	qoingrpc.RegisterTestSaseServiceServer(s, &service.TestSaseServer{})
	// qoingrpc.RegisterTestServiceServer(s, &service.TestStokServer{})
}
