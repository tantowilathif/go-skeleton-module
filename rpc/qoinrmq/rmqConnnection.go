package qoinrmq

import (
	"fmt"
	"os"
	"strconv"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"

	"github.com/streadway/amqp"
)

type QoinRmqPayload struct {
	Param interface{} `json:"param"`
	Data  interface{} `json:"data"`
}

type QoinRmq struct {
	Connection    *amqp.Connection
	Channel       *amqp.Channel
	Queue         amqp.Queue
	PrefetchCount int
	PrefetchSize  int
	Error         error
}

func (conn *QoinRmq) StartConnection() {
	conn.Connection, conn.Error = amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s%s",
		os.Getenv("QOINRMQ_USER"),
		os.Getenv("QOINRMQ_PASS"),
		os.Getenv("QOINRMQ_HOST"),
		os.Getenv("QOINRMQ_PORT"),
		os.Getenv("QOINRMQ_VHOST"),
	))
	qoinhelper.LoggerError(conn.Error)

	conn.PrefetchCount, _ = strconv.Atoi(os.Getenv("QOINRMQ_CONC_COUNT"))
	conn.PrefetchSize, _ = strconv.Atoi(os.Getenv("QOINRMQ_CONC_SIZE"))

	qoinhelper.LoggerInfo("Create Connection Success")
}

func (conn *QoinRmq) StartChannel() {
	conn.Channel, conn.Error = conn.Connection.Channel()
	qoinhelper.LoggerError(conn.Error)

	conn.Channel.Qos(
		conn.PrefetchCount, // prefetch count
		conn.PrefetchSize,  // prefetch size
		false,              // global
	)

	qoinhelper.LoggerInfo("Create Channel Success")
}

func (conn *QoinRmq) DeclareQueue(qName string) {
	conn.Queue, conn.Error = conn.Channel.QueueDeclare(qName,
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	qoinhelper.LoggerError(conn.Error)

	qoinhelper.LoggerInfo("Declare Queue Success")
}

func (conn *QoinRmq) CloseAll() {
	conn.Connection.Close()
	conn.Channel.Close()
}

func (conn *QoinRmq) Connect(qName string) {
	conn.StartConnection()
	conn.StartChannel()
	conn.DeclareQueue(qName)
}

func (conn *QoinRmq) ReConnect(qName string) {
	conn.CloseAll()
	conn.StartConnection()
	conn.StartChannel()
	conn.DeclareQueue(qName)
}
