package qoinrmq

import (
	"encoding/json"
	"fmt"
	"go-skeleton-module/service"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
)

func (rpc QoinRmq) StartConsume(qName string) {
	defer func() { recover() }()

	rpc.StartConnection()
	rpc.StartChannel()
	rpc.DeclareQueue(qName)

	forever := make(chan bool)

	qoinhelper.LoggerInfo(qName)

	msgs, err := rpc.Channel.Consume(
		qName, // queue
		"",    // consumer
		false, // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)

	qoinhelper.LoggerError(err)

	// for i := 0; i < rpc.PrefetchCount; i++ {
	go func() {
		for data := range msgs {

			var request QoinRmqPayload
			err := json.Unmarshal(data.Body, &request)

			qoinhelper.LoggerError(err)

			qoinhelper.LoggerSuccess(string(data.Body))
			qoinhelper.LoggerSuccess(fmt.Sprint(request.Param) + " == " + fmt.Sprint(request.Data))

			// Add function handler you had been create
			service.RabbitMQHandler(request.Param, request.Data)

			data.Ack(true)
		}
	}()
	// }

	qoinhelper.LoggerInfo(" [*] Awaiting RPC requests")
	<-forever
}
