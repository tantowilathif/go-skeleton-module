package rpc

import (
	"log"
	"net"
	"os"

	grpc "google.golang.org/grpc"
)

// var RpcConfig *QoinRpc

type IRpc interface {
	GrpcStartServer()
	RabbitMQRpcConnection()
}

type QoinRpc struct {
	Host    string
	Port    string
	Rpc     *IRpc
	RServer *grpc.Server
}

func (c *QoinRpc) GrpcStartServer() {

	// Init Environment
	c.StartConnection()

	listen, err := net.Listen("tcp", c.Host+":"+c.Port)
	if err != nil {
		log.Fatalf("Failed to listen : %v", err)
	}

	c.RServer = grpc.NewServer()
	RouteGrpcServer(c.RServer)

	// start gRPC server
	log.Println("starting gRPC server ...")
	log.Println("gRpc Port :", c.Port)
	log.Println("gRpc Host :", c.Host)
	if err := c.RServer.Serve(listen); err != nil {
		log.Fatalf("Failed to listen : %v", err)
	}
}

func (c *QoinRpc) StartConnection() {
	c.Host = os.Getenv("GRPC_HOST")
	c.Port = os.Getenv("GRPC_PORT")
}
