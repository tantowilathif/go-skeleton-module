# Grpc Golang

This folder is repository for [GRPC Server](https://grpc.io/)

[![logo](https://avatars.githubusercontent.com/u/72009988?s=200&v=4)](https://www.qoin.id/)

# Dir

- Routes 
- Proto
- type of gRPC Unary, Server Streaming, Client Streaming, BiDi Streaming
- Handler to controller / models


to Generate proto
- use cmd "protoc --go_out=. --go_opt=paths=source_relative rpc/qoingrpc/qoin.prot"
- use cmd "protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative rpc/qoingrpc/transaksi.proto"

Remove omitempty
- use cmd "ls ./rpc/qoingrpc/qoin.pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'"
