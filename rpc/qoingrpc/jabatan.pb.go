// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.19.1
// source: rpc/qoingrpc/jabatan.proto

package qoingrpc

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Jabatan struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id   int64  `protobuf:"varint,1,opt,name=Id,proto3" json:"Id,omitempty"`
	Name string `protobuf:"bytes,2,opt,name=Name,proto3" json:"Name,omitempty"`
}

func (x *Jabatan) Reset() {
	*x = Jabatan{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_jabatan_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Jabatan) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Jabatan) ProtoMessage() {}

func (x *Jabatan) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_jabatan_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Jabatan.ProtoReflect.Descriptor instead.
func (*Jabatan) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_jabatan_proto_rawDescGZIP(), []int{0}
}

func (x *Jabatan) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *Jabatan) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

type JabatanRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Jabatan *Jabatan `protobuf:"bytes,1,opt,name=jabatan,proto3" json:"jabatan,omitempty"`
}

func (x *JabatanRequest) Reset() {
	*x = JabatanRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_jabatan_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *JabatanRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*JabatanRequest) ProtoMessage() {}

func (x *JabatanRequest) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_jabatan_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use JabatanRequest.ProtoReflect.Descriptor instead.
func (*JabatanRequest) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_jabatan_proto_rawDescGZIP(), []int{1}
}

func (x *JabatanRequest) GetJabatan() *Jabatan {
	if x != nil {
		return x.Jabatan
	}
	return nil
}

type JabatanResponses struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32    `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string   `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	Data    *Jabatan `protobuf:"bytes,3,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *JabatanResponses) Reset() {
	*x = JabatanResponses{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_jabatan_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *JabatanResponses) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*JabatanResponses) ProtoMessage() {}

func (x *JabatanResponses) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_jabatan_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use JabatanResponses.ProtoReflect.Descriptor instead.
func (*JabatanResponses) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_jabatan_proto_rawDescGZIP(), []int{2}
}

func (x *JabatanResponses) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *JabatanResponses) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *JabatanResponses) GetData() *Jabatan {
	if x != nil {
		return x.Data
	}
	return nil
}

type JabatanListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32      `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string     `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	List    []*Jabatan `protobuf:"bytes,3,rep,name=list,proto3" json:"list,omitempty"`
}

func (x *JabatanListResponse) Reset() {
	*x = JabatanListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_rpc_qoingrpc_jabatan_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *JabatanListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*JabatanListResponse) ProtoMessage() {}

func (x *JabatanListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_rpc_qoingrpc_jabatan_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use JabatanListResponse.ProtoReflect.Descriptor instead.
func (*JabatanListResponse) Descriptor() ([]byte, []int) {
	return file_rpc_qoingrpc_jabatan_proto_rawDescGZIP(), []int{3}
}

func (x *JabatanListResponse) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *JabatanListResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *JabatanListResponse) GetList() []*Jabatan {
	if x != nil {
		return x.List
	}
	return nil
}

var File_rpc_qoingrpc_jabatan_proto protoreflect.FileDescriptor

var file_rpc_qoingrpc_jabatan_proto_rawDesc = []byte{
	0x0a, 0x1a, 0x72, 0x70, 0x63, 0x2f, 0x71, 0x6f, 0x69, 0x6e, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x6a,
	0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07, 0x6a, 0x61,
	0x62, 0x61, 0x74, 0x61, 0x6e, 0x22, 0x2d, 0x0a, 0x07, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e,
	0x12, 0x0e, 0x0a, 0x02, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x49, 0x64,
	0x12, 0x12, 0x0a, 0x04, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04,
	0x4e, 0x61, 0x6d, 0x65, 0x22, 0x3c, 0x0a, 0x0e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x2a, 0x0a, 0x07, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61,
	0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x10, 0x2e, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61,
	0x6e, 0x2e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x07, 0x6a, 0x61, 0x62, 0x61, 0x74,
	0x61, 0x6e, 0x22, 0x66, 0x0a, 0x10, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73,
	0x73, 0x61, 0x67, 0x65, 0x12, 0x24, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x10, 0x2e, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a, 0x61, 0x62,
	0x61, 0x74, 0x61, 0x6e, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x69, 0x0a, 0x13, 0x4a, 0x61,
	0x62, 0x61, 0x74, 0x61, 0x6e, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52,
	0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12,
	0x24, 0x0a, 0x04, 0x6c, 0x69, 0x73, 0x74, 0x18, 0x03, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x10, 0x2e,
	0x6a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52,
	0x04, 0x6c, 0x69, 0x73, 0x74, 0x32, 0xe9, 0x02, 0x0a, 0x0e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61,
	0x6e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x3f, 0x0a, 0x07, 0x4a, 0x61, 0x62, 0x61,
	0x74, 0x61, 0x6e, 0x12, 0x17, 0x2e, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a, 0x61,
	0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x6a,
	0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x22, 0x00, 0x12, 0x42, 0x0a, 0x0a, 0x47, 0x65, 0x74,
	0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x12, 0x17, 0x2e, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61,
	0x6e, 0x2e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x19, 0x2e, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a, 0x61, 0x62, 0x61, 0x74,
	0x61, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x22, 0x00, 0x12, 0x46, 0x0a,
	0x0b, 0x4c, 0x69, 0x73, 0x74, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x12, 0x17, 0x2e, 0x6a,
	0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1c, 0x2e, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e,
	0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x43, 0x0a, 0x0b, 0x45, 0x64, 0x69, 0x74, 0x4a, 0x61, 0x62,
	0x61, 0x74, 0x61, 0x6e, 0x12, 0x17, 0x2e, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a,
	0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e,
	0x6a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x22, 0x00, 0x12, 0x45, 0x0a, 0x0d, 0x44, 0x65,
	0x6c, 0x65, 0x74, 0x65, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x12, 0x17, 0x2e, 0x6a, 0x61,
	0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x6a, 0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x2e, 0x4a,
	0x61, 0x62, 0x61, 0x74, 0x61, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x73, 0x22,
	0x00, 0x42, 0x0e, 0x5a, 0x0c, 0x72, 0x70, 0x63, 0x2f, 0x71, 0x6f, 0x69, 0x6e, 0x67, 0x72, 0x70,
	0x63, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_rpc_qoingrpc_jabatan_proto_rawDescOnce sync.Once
	file_rpc_qoingrpc_jabatan_proto_rawDescData = file_rpc_qoingrpc_jabatan_proto_rawDesc
)

func file_rpc_qoingrpc_jabatan_proto_rawDescGZIP() []byte {
	file_rpc_qoingrpc_jabatan_proto_rawDescOnce.Do(func() {
		file_rpc_qoingrpc_jabatan_proto_rawDescData = protoimpl.X.CompressGZIP(file_rpc_qoingrpc_jabatan_proto_rawDescData)
	})
	return file_rpc_qoingrpc_jabatan_proto_rawDescData
}

var file_rpc_qoingrpc_jabatan_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_rpc_qoingrpc_jabatan_proto_goTypes = []interface{}{
	(*Jabatan)(nil),             // 0: jabatan.Jabatan
	(*JabatanRequest)(nil),      // 1: jabatan.JabatanRequest
	(*JabatanResponses)(nil),    // 2: jabatan.JabatanResponses
	(*JabatanListResponse)(nil), // 3: jabatan.JabatanListResponse
}
var file_rpc_qoingrpc_jabatan_proto_depIdxs = []int32{
	0, // 0: jabatan.JabatanRequest.jabatan:type_name -> jabatan.Jabatan
	0, // 1: jabatan.JabatanResponses.data:type_name -> jabatan.Jabatan
	0, // 2: jabatan.JabatanListResponse.list:type_name -> jabatan.Jabatan
	1, // 3: jabatan.JabatanService.Jabatan:input_type -> jabatan.JabatanRequest
	1, // 4: jabatan.JabatanService.GetJabatan:input_type -> jabatan.JabatanRequest
	1, // 5: jabatan.JabatanService.ListJabatan:input_type -> jabatan.JabatanRequest
	1, // 6: jabatan.JabatanService.EditJabatan:input_type -> jabatan.JabatanRequest
	1, // 7: jabatan.JabatanService.DeleteJabatan:input_type -> jabatan.JabatanRequest
	2, // 8: jabatan.JabatanService.Jabatan:output_type -> jabatan.JabatanResponses
	2, // 9: jabatan.JabatanService.GetJabatan:output_type -> jabatan.JabatanResponses
	3, // 10: jabatan.JabatanService.ListJabatan:output_type -> jabatan.JabatanListResponse
	2, // 11: jabatan.JabatanService.EditJabatan:output_type -> jabatan.JabatanResponses
	2, // 12: jabatan.JabatanService.DeleteJabatan:output_type -> jabatan.JabatanResponses
	8, // [8:13] is the sub-list for method output_type
	3, // [3:8] is the sub-list for method input_type
	3, // [3:3] is the sub-list for extension type_name
	3, // [3:3] is the sub-list for extension extendee
	0, // [0:3] is the sub-list for field type_name
}

func init() { file_rpc_qoingrpc_jabatan_proto_init() }
func file_rpc_qoingrpc_jabatan_proto_init() {
	if File_rpc_qoingrpc_jabatan_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_rpc_qoingrpc_jabatan_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Jabatan); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_rpc_qoingrpc_jabatan_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*JabatanRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_rpc_qoingrpc_jabatan_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*JabatanResponses); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_rpc_qoingrpc_jabatan_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*JabatanListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_rpc_qoingrpc_jabatan_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_rpc_qoingrpc_jabatan_proto_goTypes,
		DependencyIndexes: file_rpc_qoingrpc_jabatan_proto_depIdxs,
		MessageInfos:      file_rpc_qoingrpc_jabatan_proto_msgTypes,
	}.Build()
	File_rpc_qoingrpc_jabatan_proto = out.File
	file_rpc_qoingrpc_jabatan_proto_rawDesc = nil
	file_rpc_qoingrpc_jabatan_proto_goTypes = nil
	file_rpc_qoingrpc_jabatan_proto_depIdxs = nil
}
