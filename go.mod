module go-skeleton-module

go 1.16

require (
	github.com/Qoin-Digital-Indonesia/qoingohelper v0.0.0-20210723211546-f14211e2f654
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/streadway/amqp v1.0.0
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.22.3
)
