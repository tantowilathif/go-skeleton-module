package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type PembelianServer struct {
	qoingrpc.UnimplementedPembelianServiceServer
	qoingrpc.PembelianListResponse
	qoingrpc.PembelianResponses
}

func (c *PembelianServer) Pembelian(ctx context.Context, req *qoingrpc.PembelianRequest) (rsp *qoingrpc.PembelianResponses, err error) {
	PembelianModel := models.Pembelian{
		Tanggal:     req.Pembelian.Tanggal,
		Supplier: 	req.Pembelian.Supplier,
		Detail: 	req.Pembelian.Detail,
	}

	res, err := PembelianModel.Add()

	if err != nil || res == nil {
		fmt.Printf("failed to save Pembelian data %v", err)
		return &qoingrpc.PembelianResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PembelianResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pembelian{
			Id:        int32(res.Id),
			Tanggal:      res.Tanggal,
			Supplier: res.Supplier,
		},
	}, nil
}
func (*PembelianServer) GetPembelian(ctx context.Context, req *qoingrpc.PembelianRequest) (rsp *qoingrpc.PembelianResponses, err error) {

	PembelianModel := models.Pembelian{
		Id: int32(req.Pembelian.Id),
	}

	res, err := PembelianModel.Get()

	if err != nil {
		fmt.Printf("failed to save Pembelian data %v", err)
		return &qoingrpc.PembelianResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PembelianResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pembelian{
			Id:        int32(res.Id),
			Tanggal:      res.Tanggal,
			Supplier: res.Supplier,
		},
	}, nil
}

func (*PembelianServer) ListPembelian(ctx context.Context, req *qoingrpc.PembelianRequest) (rsp *qoingrpc.PembelianListResponse, err error) {

	PembelianModel := models.Pembelian{
		Id: int32(req.Pembelian.Id),
		Filter: req.Pembelian.Filter,
		Offset: req.Pembelian.Offset,
		Limit:  req.Pembelian.Limit,

	}

	res, err := PembelianModel.List()

	if err != nil {
		fmt.Printf("failed to save Pembelian data %v", err)
		return &qoingrpc.PembelianListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	//var listData = make([]*qoingrpc.Pembelian, len(res.List))
	//for i, val := range res.List {
	//	listData[i] = &qoingrpc.Pembelian{
	//		Id:        int32(val.Id),
	//		Tanggal:    val.Tanggal,
	//		Supplier: 	val.Supplier,
	//	}
	//}

	return &qoingrpc.PembelianListResponse{
		Code:    200,
		Message: "success",
		List:    res,
	}, nil
}

func (*PembelianServer) ListDetailPembelian(ctx context.Context, req *qoingrpc.PembelianRequest) (rsp *qoingrpc.PembelianListResponse, err error) {

	PembelianDetailModel := models.Pembelian{
		Id: int32(req.Pembelian.Id),
	}

	_,res, err := PembelianDetailModel.ListDetail()

	if err != nil {
		fmt.Printf("failed to save PembelianDetail data %v", err)
		return &qoingrpc.PembelianListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var listData = make([]*qoingrpc.PembelianDetail, len(res))
	for i, val := range res {
		listData[i] = &qoingrpc.PembelianDetail{
			Id:        int32(val.Id),
			PembelianId  : int32(val.PembelianId),
			BarangId : int32(val.BarangId),
			Nama : val.Nama,
			Harga : float32(val.Harga),
			Jumlah : float32(val.Jumlah),
			Total : float32(val.Total),
		}
	}

	return &qoingrpc.PembelianListResponse{
		Code:    200,
		Message: "success",
		List:  nil,
		ListDetail:  listData,
	}, nil

}

func (*PembelianServer) EditPembelian(ctx context.Context, req *qoingrpc.PembelianRequest) (rsp *qoingrpc.PembelianResponses, err error) {
	PembelianModel := models.Pembelian{
		Id:        int32(req.Pembelian.Id),
		Tanggal:      req.Pembelian.Tanggal,
		Supplier: req.Pembelian.Supplier,
		Detail: 	req.Pembelian.Detail,
	}

	res, err := PembelianModel.Update()

	if err != nil || res == nil {
		fmt.Printf("failed to save Pembelian data %v", err)
		return &qoingrpc.PembelianResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PembelianResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pembelian{
			Id:        int32(res.Id),
			Tanggal:      res.Tanggal,
			Supplier: res.Supplier,
		},
	}, nil
}
func (*PembelianServer) DeletePembelian(ctx context.Context, req *qoingrpc.PembelianRequest) (rsp *qoingrpc.PembelianResponses, err error) {
	PembelianModel := models.Pembelian{
		Id: int32(req.Pembelian.Id),
	}

	_, err = PembelianModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Pembelian data %v", err)
		return &qoingrpc.PembelianResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PembelianResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
