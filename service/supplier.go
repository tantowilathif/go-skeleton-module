package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type SupplierServer struct {
	qoingrpc.UnimplementedSupplierServiceServer
	qoingrpc.SupplierListResponse
	qoingrpc.SupplierResponses
}

func (c *SupplierServer) Supplier(ctx context.Context, req *qoingrpc.SupplierRequest) (rsp *qoingrpc.SupplierResponses, err error) {
	SupplierModel := models.Supplier{
		Nama:   req.Supplier.Nama,
		NoTelp: req.Supplier.NoTelp,
		Email:  req.Supplier.Email,
		Alamat: req.Supplier.Alamat,
		Detail: req.Supplier.Detail,
	}
	splrData, err := SupplierModel.Add()

	if err != nil {
		fmt.Printf("failed to save Supplier data %v", err)
		return &qoingrpc.SupplierResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.SupplierResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Supplier{
			Id:     int64(splrData.Id),
			Nama:   splrData.Nama,
			NoTelp: splrData.NoTelp,
			Email:  splrData.Email,
			Alamat: splrData.Alamat,
		},
	}, nil
}
func (*SupplierServer) GetSupplier(ctx context.Context, req *qoingrpc.SupplierRequest) (rsp *qoingrpc.SupplierResponses, err error) {

	SupplierModel := models.Supplier{
		Id: int64(req.Supplier.Id),
	}

	splrData, err := SupplierModel.Get()

	if err != nil {
		fmt.Printf("failed to save Supplier data %v", err)
		return &qoingrpc.SupplierResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.SupplierResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Supplier{
			Id:     int64(splrData.Id),
			Nama:   splrData.Nama,
			NoTelp: splrData.NoTelp,
			Email:  splrData.Email,
			Alamat: splrData.Alamat,
		},
	}, nil
}

func (*SupplierServer) ListSupplier(ctx context.Context, req *qoingrpc.SupplierRequest) (rsp *qoingrpc.SupplierListResponse, err error) {

	SupplierModel := models.Supplier{
		Id:     int64(req.Supplier.Id),
		Filter: req.Supplier.Filter,
		Offset: req.Supplier.Offset,
		Limit:  req.Supplier.Limit,
	}

	data, err := SupplierModel.List()
	if err != nil {
		fmt.Printf("failed to save Supplier data %v", err)
		return &qoingrpc.SupplierListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	// var dataSupplier = make([]*qoingrpc.Supplier, len(data.List))
	// for i, val := range data.List {
	// 	dataSupplier[i] = &qoingrpc.Supplier{
	// 		Id:     int64(val.Id),
	// 		Nama:   val.Nama,
	// 		Email:  val.Email,
	// 		NoTelp: val.NoTelp,
	// 		Alamat: val.NoTelp,
	// 	}
	// }

	// data.List = dataSupplier

	return &qoingrpc.SupplierListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}

func (*SupplierServer) ListDetailSupplier(ctx context.Context, req *qoingrpc.SupplierRequest) (rsp *qoingrpc.SupplierListResponse, err error) {

	SupplierDetailModel := models.Supplier{
		Id: int64(req.Supplier.Id),
	}

	_, res, err := SupplierDetailModel.ListDetail()

	if err != nil {
		fmt.Printf("failed to save SupplierDetail data %v", err)
		return &qoingrpc.SupplierListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var listData = make([]*qoingrpc.SupplierDetail, len(res))
	for i, val := range res {
		listData[i] = &qoingrpc.SupplierDetail{
			Id:            int32(val.Id),
			SupplierId:    int32(val.SupplierId),
			ContactPerson: val.ContactPerson,
			NoTelp:        val.NoTelp,
			Bank:          val.Bank,
			NamaRekening:  val.NamaRekening,
			NomorRekening: val.NomorRekening,
		}
	}

	return &qoingrpc.SupplierListResponse{
		Code:       200,
		Message:    "success",
		List:       nil,
		ListDetail: listData,
	}, nil

}

func (*SupplierServer) EditSupplier(ctx context.Context, req *qoingrpc.SupplierRequest) (rsp *qoingrpc.SupplierResponses, err error) {
	SupplierModel := models.Supplier{
		Id:     int64(req.Supplier.Id),
		Nama:   req.Supplier.Nama,
		Email:  req.Supplier.Email,
		NoTelp: req.Supplier.NoTelp,
		Alamat: req.Supplier.Alamat,
		Detail: req.Supplier.Detail,
	}

	brgData, err := SupplierModel.Update()

	if err != nil || brgData == nil {
		fmt.Printf("failed to save Supplier data %v", err)
		return &qoingrpc.SupplierResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.SupplierResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Supplier{
			Id:     int64(brgData.Id),
			Nama:   brgData.Nama,
			Email:  brgData.Email,
			NoTelp: brgData.NoTelp,
			Alamat: brgData.Alamat,
		},
	}, nil
}
func (*SupplierServer) DeleteSupplier(ctx context.Context, req *qoingrpc.SupplierRequest) (rsp *qoingrpc.SupplierResponses, err error) {
	SupplierModel := models.Supplier{
		Id: int64(req.Supplier.Id),
	}

	_, err = SupplierModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Supplier data %v", err)
		return &qoingrpc.SupplierResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.SupplierResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
