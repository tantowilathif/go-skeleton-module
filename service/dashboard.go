package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type DashboardServer struct {
	qoingrpc.UnimplementedDashboardServiceServer
	qoingrpc.DashboardListResponse
	qoingrpc.DashboardResponses

	// Dashboard Grafik
	qoingrpc.DashboardGrafikResponses
}

func (*DashboardServer) DashboardGrafik(ctx context.Context, req *qoingrpc.DashboardGrafikRequest) (rsp *qoingrpc.DashboardGrafikResponses, err error) {
	DashboardGrafikModel := models.Dashboard{
		UserId:         req.DashboardGrafik.UserId,
		Tipe:           req.DashboardGrafik.Tipe,
		TanggalMulai:   req.DashboardGrafik.TanggalMulai,
		TanggalSelesai: req.DashboardGrafik.TanggalSelesai,
	}

	dsbGfkData, err := DashboardGrafikModel.DashboardGrafik()
	if err != nil {
		fmt.Printf("failed to get Dashboard Grafik data %v", err)
		return &qoingrpc.DashboardGrafikResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.DashboardGrafikResponses{
		Code:    200,
		Message: "success",
		Data:    dsbGfkData,
	}, nil

}

func (*DashboardServer) LapKeuangan(ctx context.Context, req *qoingrpc.DashboardRequest) (rsp *qoingrpc.DashboardListResponse, err error) {

	DashboardModel := models.Dashboard{
		UserId:         req.Dashboard.UserId,
		TanggalMulai:   req.Dashboard.TanggalMulai,
		TanggalSelesai: req.Dashboard.TanggalSelesai,
	}

	data, err := DashboardModel.LapKeuangan()
	if err != nil {
		fmt.Printf("failed to save Dashboard} data %v", err)
		return &qoingrpc.DashboardListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}
	return &qoingrpc.DashboardListResponse{
		Code:        200,
		Message:     "success",
		List:        nil,
		LapKeuangan: data,
	}, nil
}
