package service

import (
	"context"
	"fmt"

	// "go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type CustomerServer struct {
	qoingrpc.UnimplementedCustomerServiceServer
	qoingrpc.CustomerListResponse
	qoingrpc.CustomerResponses
}

func (c *CustomerServer) Customer(ctx context.Context, req *qoingrpc.CustomerRequest) (rsp *qoingrpc.CustomerResponses, err error) {
	CustomerModel := models.Customer{
		Nama:   req.Customer.Nama,
		NoTelp: req.Customer.NoTelp,
		Email:  req.Customer.Email,
		Alamat: req.Customer.Alamat,
		Detail: req.Customer.Detail,
	}
	CusData, err := CustomerModel.Add()

	if err != nil {
		fmt.Printf("failed to save Customer data %v", err)
		return &qoingrpc.CustomerResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.CustomerResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Customer{
			Id:     int32(CusData.Id),
			Nama:   CusData.Nama,
			NoTelp: CusData.NoTelp,
			Email:  CusData.Email,
			Alamat: CusData.Alamat,
		},
	}, nil
}
func (*CustomerServer) GetCustomer(ctx context.Context, req *qoingrpc.CustomerRequest) (rsp *qoingrpc.CustomerResponses, err error) {

	CustomerModel := models.Customer{
		Id: int32(req.Customer.Id),
	}

	CusData, err := CustomerModel.Get()

	if err != nil {
		fmt.Printf("failed to save Customer data %v", err)
		return &qoingrpc.CustomerResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.CustomerResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Customer{
			Id:     int32(CusData.Id),
			Nama:   CusData.Nama,
			NoTelp: CusData.NoTelp,
			Email:  CusData.Email,
			Alamat: CusData.Alamat,
		},
	}, nil
}

func (*CustomerServer) ListCustomer(ctx context.Context, req *qoingrpc.CustomerRequest) (rsp *qoingrpc.CustomerListResponse, err error) {

	CustomerModel := models.Customer{
		Id:     int32(req.Customer.Id),
		Filter: req.Customer.Filter,
		Offset: req.Customer.Offset,
		Limit:  req.Customer.Limit,
	}

	data, err := CustomerModel.List()
	if err != nil {
		fmt.Printf("failed to save Customer data %v", err)
		return &qoingrpc.CustomerListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var dataCustomer = make([]*qoingrpc.Customer, len(data.List))
	for i, val := range data.List {

		/*
			MENCARI SISA STOK
		*/
		// getStok := global.SisaStok{
		// 	Customer_Id: val.Id,
		// }
		// sisaStok, _ := getStok.GetSisaStok()

		//fmt.Println(sisaStok)

		dataCustomer[i] = &qoingrpc.Customer{
			Id:     int32(val.Id),
			Nama:   val.Nama,
			NoTelp: val.NoTelp,
			Email:  val.Email,
			Alamat: val.Alamat,
		}
	}

	data.List = dataCustomer

	return &qoingrpc.CustomerListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}

func (*CustomerServer) ListDetailCustomer(ctx context.Context, req *qoingrpc.CustomerRequest) (rsp *qoingrpc.CustomerListResponse, err error) {

	CustomerDetailModel := models.Customer{
		Id: int32(req.Customer.Id),
	}

	_,res, err := CustomerDetailModel.ListDetail()

	if err != nil {
		fmt.Printf("failed to save PembelianDetail data %v", err)
		return &qoingrpc.CustomerListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var listData = make([]*qoingrpc.CustomerDetail, len(res))
	for i, val := range res {
		listData[i] = &qoingrpc.CustomerDetail{
			Id:        int32(val.Id),
			CustomerId  : int32(val.CustomerId),
			ContactPerson : val.ContactPerson,
			NoTelp : int32(val.NoTelp),
			Bank : val.Bank,
			NamaRekening : val.NamaRekening,
			NomerRekening : int32(val.NomerRekening),
		}
	}

	return &qoingrpc.CustomerListResponse{
		Code:    200,
		Message: "success",
		List:  nil,
		ListDetail:  listData,
	}, nil

}

func (*CustomerServer) EditCustomer(ctx context.Context, req *qoingrpc.CustomerRequest) (rsp *qoingrpc.CustomerResponses, err error) {
	CustomerModel := models.Customer{
		Id:     int32(req.Customer.Id),
		Nama:   req.Customer.Nama,
		NoTelp: req.Customer.NoTelp,
		Email:  req.Customer.Email,
		Alamat: req.Customer.Alamat,
	}

	CusData, err := CustomerModel.Update()

	if err != nil || CusData == nil {
		fmt.Printf("failed to save Customer data %v", err)
		return &qoingrpc.CustomerResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.CustomerResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Customer{
			Id:     int32(CusData.Id),
			Nama:   CusData.Nama,
			NoTelp: CusData.NoTelp,
			Email:  CusData.Email,
			Alamat: CusData.Alamat,
		},
	}, nil
}
func (*CustomerServer) DeleteCustomer(ctx context.Context, req *qoingrpc.CustomerRequest) (rsp *qoingrpc.CustomerResponses, err error) {
	CustomerModel := models.Customer{
		Id: int32(req.Customer.Id),
	}

	_, err = CustomerModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Customer data %v", err)
		return &qoingrpc.CustomerResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.CustomerResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
