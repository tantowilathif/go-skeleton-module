package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type PenjualanServer struct {
	qoingrpc.UnimplementedPenjualanServiceServer
	qoingrpc.PenjualanListResponse
	qoingrpc.PenjualanResponses
}

func (c *PenjualanServer) Penjualan(ctx context.Context, req *qoingrpc.PenjualanRequest) (rsp *qoingrpc.PenjualanResponses, err error) {
	PenjualanModel := models.Penjualan{
		Tanggal:      req.Penjualan.Tanggal,
		TotalHarga:      req.Penjualan.TotalHarga,
		TotalBayar:      req.Penjualan.TotalBayar,
		Kembalian:      req.Penjualan.Kembalian,
		StatusLunas:      req.Penjualan.StatusLunas,
		Detail: req.Penjualan.Detail,
	}

	res, err := PenjualanModel.Add()

	if err != nil {
		fmt.Printf("failed to save Penjualan data %v", err)
		return &qoingrpc.PenjualanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PenjualanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Penjualan{
			Id:        int32(res.Id),
			Tanggal:     res.Tanggal,
			TotalHarga:     res.TotalHarga,
			TotalBayar:     res.TotalBayar,
			Kembalian:     res.Kembalian,
			StatusLunas:     res.StatusLunas,
		},
	}, nil
}
func (*PenjualanServer) GetPenjualan(ctx context.Context, req *qoingrpc.PenjualanRequest) (rsp *qoingrpc.PenjualanResponses, err error) {

	PenjualanModel := models.Penjualan{
		Id: int32(req.Penjualan.Id),
	}

	res, err := PenjualanModel.Get()

	if err != nil {
		fmt.Printf("failed to save Penjualan data %v", err)
		return &qoingrpc.PenjualanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PenjualanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Penjualan{
			Id:        int32(res.Id),
			Tanggal:      res.Tanggal,
			TotalHarga:      res.TotalHarga,
			TotalBayar:      res.TotalBayar,
			Kembalian:      res.Kembalian,
			StatusLunas:      res.StatusLunas,
		},
	}, nil
}

func (*PenjualanServer) ListPenjualan(ctx context.Context, req *qoingrpc.PenjualanRequest) (rsp *qoingrpc.PenjualanListResponse, err error) {

	PenjualanModel := models.Penjualan{
		Id: int32(req.Penjualan.Id),
		Filter: req.Penjualan.Filter,
		Offset: req.Penjualan.Offset,
		Limit:  req.Penjualan.Limit,
	}

	res, err := PenjualanModel.List()

	if err != nil {
		fmt.Printf("failed to save Penjualan data %v", err)
		return &qoingrpc.PenjualanListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	//var listData = make([]*qoingrpc.Penjualan, len(res.List))
	//for i, val := range res.List {
	//	listData[i] = &qoingrpc.Penjualan{
	//		Id:        int32(val.Id),
	//		Tanggal:      val.Tanggal,
	//		TotalHarga:      val.TotalHarga,
	//		TotalBayar:      val.TotalBayar,
	//		Kembalian:      val.Kembalian,
	//		StatusLunas:      val.StatusLunas,
	//	}
	//}

	return &qoingrpc.PenjualanListResponse{
		Code:    200,
		Message: "success",
		List:    res,
	}, nil
}

func (*PenjualanServer) ListDetailPenjualan(ctx context.Context, req *qoingrpc.PenjualanRequest) (rsp *qoingrpc.PenjualanListResponse, err error) {

	PenjualanDetailModel := models.Penjualan{
		Id: int32(req.Penjualan.Id),
	}

	_,res, err := PenjualanDetailModel.ListDetail()

	if err != nil {
		fmt.Printf("failed to save PenjualanDetail data %v", err)
		return &qoingrpc.PenjualanListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var listData = make([]*qoingrpc.PenjualanDetail, len(res))
	for i, val := range res {
		listData[i] = &qoingrpc.PenjualanDetail{
			Id:        int32(val.Id),
			PenjualanId  : int32(val.PenjualanId),
			BarangId : int32(val.BarangId),
			Nama : val.Nama,
			Harga : float32(val.Harga),
			Jumlah : int32(val.Jumlah),
			Total : float32(val.Total),
		}
	}
	return &qoingrpc.PenjualanListResponse{
		Code:    200,
		Message: "success",
		List:  nil,
		ListDetail:  listData,
	}, nil

}

func (*PenjualanServer) EditPenjualan(ctx context.Context, req *qoingrpc.PenjualanRequest) (rsp *qoingrpc.PenjualanResponses, err error) {
	PenjualanModel := models.Penjualan{
		Id:        int32(req.Penjualan.Id),
		Tanggal:      req.Penjualan.Tanggal,
		TotalHarga:      req.Penjualan.TotalHarga,
		TotalBayar:      req.Penjualan.TotalBayar,
		Kembalian:      req.Penjualan.Kembalian,
		StatusLunas:      req.Penjualan.StatusLunas,
		Detail: req.Penjualan.Detail,
	}

	res, err := PenjualanModel.Update()

	if err != nil || res == nil {
		fmt.Printf("failed to save Penjualan data %v", err)
		return &qoingrpc.PenjualanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PenjualanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Penjualan{
			Id:        int32(res.Id),
			Tanggal:      res.Tanggal,
			TotalHarga:      res.TotalHarga,
			TotalBayar:      res.TotalBayar,
			Kembalian:      res.Kembalian,
			StatusLunas:      res.StatusLunas,
		},
	}, nil
}
func (*PenjualanServer) DeletePenjualan(ctx context.Context, req *qoingrpc.PenjualanRequest) (rsp *qoingrpc.PenjualanResponses, err error) {
	PenjualanModel := models.Penjualan{
		Id: int32(req.Penjualan.Id),
	}

	_, err = PenjualanModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Penjualan data %v", err)
		return &qoingrpc.PenjualanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PenjualanResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
