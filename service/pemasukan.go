package service

import (
	"context"
	"fmt"

	// "go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type PemasukanServer struct {
	qoingrpc.UnimplementedPemasukanServiceServer
	qoingrpc.PemasukanListResponse
	qoingrpc.PemasukanResponses
}

func (c *PemasukanServer) Pemasukan(ctx context.Context, req *qoingrpc.PemasukanRequest) (rsp *qoingrpc.PemasukanResponses, err error) {
	PemasukanModel := models.Pemasukan{
		Jumlah:   float32(req.Pemasukan.Jumlah),
		Tanggal: req.Pemasukan.Tanggal,
		Keterangan:  req.Pemasukan.Keterangan,
		KategoriId: int32(req.Pemasukan.KategoriId),
	}
	PemData, err := PemasukanModel.Add()

	if err != nil {
		fmt.Printf("failed to save Pemasukan data %v", err)
		return &qoingrpc.PemasukanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PemasukanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pemasukan{
			Id:     int32(PemData.Id),
			Jumlah:   float32(PemData.Jumlah),
			Tanggal: PemData.Tanggal,
			Keterangan:  PemData.Keterangan,
			KategoriId: int32(PemData.KategoriId),
		},
	}, nil
}
func (*PemasukanServer) GetPemasukan(ctx context.Context, req *qoingrpc.PemasukanRequest) (rsp *qoingrpc.PemasukanResponses, err error) {

	PemasukanModel := models.Pemasukan{
		Id: int32(req.Pemasukan.Id),
	}

	PemData, err := PemasukanModel.Get()

	if err != nil {
		fmt.Printf("failed to save Pemasukan data %v", err)
		return &qoingrpc.PemasukanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PemasukanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pemasukan{
			Id:     int32(PemData.Id),
			Jumlah:   float32(PemData.Jumlah),
			Tanggal: PemData.Tanggal,
			Keterangan:  PemData.Keterangan,
			KategoriId: int32(PemData.KategoriId),
		},
	}, nil
}

func (*PemasukanServer) ListPemasukan(ctx context.Context, req *qoingrpc.PemasukanRequest) (rsp *qoingrpc.PemasukanListResponse, err error) {

	PemasukanModel := models.Pemasukan{
		Id:     int32(req.Pemasukan.Id),
		Filter: req.Pemasukan.Filter,
		Offset: req.Pemasukan.Offset,
		Limit:  req.Pemasukan.Limit,
	}

	data, err := PemasukanModel.List()
	if err != nil {
		fmt.Printf("failed to save Pemasukan data %v", err)
		return &qoingrpc.PemasukanListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var dataPemasukan = make([]*qoingrpc.Pemasukan, len(data.List))
	for i, val := range data.List {

		/*
			MENCARI SISA STOK
		*/
		// getStok := global.SisaStok{
		// 	Pemasukan_Id: val.Id,
		// }
		// sisaStok, _ := getStok.GetSisaStok()

		//fmt.Println(sisaStok)

		dataPemasukan[i] = &qoingrpc.Pemasukan{
			Id:     int32(val.Id),
			Jumlah:   float32(val.Jumlah),
			Tanggal: val.Tanggal,
			Keterangan:  val.Keterangan,
			KategoriId: int32(val.KategoriId),
		}
	}

	data.List = dataPemasukan

	return &qoingrpc.PemasukanListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}

func (*PemasukanServer) EditPemasukan(ctx context.Context, req *qoingrpc.PemasukanRequest) (rsp *qoingrpc.PemasukanResponses, err error) {
	PemasukanModel := models.Pemasukan{
		Id:     int32(req.Pemasukan.Id),
		Jumlah:   float32(req.Pemasukan.Jumlah),
		Tanggal: req.Pemasukan.Tanggal,
		Keterangan:  req.Pemasukan.Keterangan,
		KategoriId: int32(req.Pemasukan.KategoriId),
	}

	PemData, err := PemasukanModel.Update()

	if err != nil || PemData == nil {
		fmt.Printf("failed to save Pemasukan data %v", err)
		return &qoingrpc.PemasukanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PemasukanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pemasukan{
			Id:     int32(PemData.Id),
			Jumlah:   float32(PemData.Jumlah),
			Tanggal: PemData.Tanggal,
			Keterangan:  PemData.Keterangan,
			KategoriId: int32(PemData.KategoriId),
		},
	}, nil
}
func (*PemasukanServer) DeletePemasukan(ctx context.Context, req *qoingrpc.PemasukanRequest) (rsp *qoingrpc.PemasukanResponses, err error) {
	PemasukanModel := models.Pemasukan{
		Id: int32(req.Pemasukan.Id),
	}

	_, err = PemasukanModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Pemasukan data %v", err)
		return &qoingrpc.PemasukanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PemasukanResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
