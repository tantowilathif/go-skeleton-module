package service

import (
	"context"
	"fmt"
	"go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
	"os"
)

type UserServer struct {
	qoingrpc.UnimplementedUserServiceServer
	qoingrpc.UserListResponse
	qoingrpc.UserResponses
}

func (c *UserServer) User(ctx context.Context, req *qoingrpc.UserRequest) (rsp *qoingrpc.UserResponses, err error) {
	UserModel := models.User{
		Name:     req.User.Name,
		Email:    req.User.Email,
		Username: req.User.Username,
		Password: global.GetMD5Hash(req.User.Password),
		Path:     req.User.Path,
		Foto:     req.User.Foto,
	}

	usrData, err := UserModel.Add()

	if err != nil {
		fmt.Printf("failed to save user data %v", err)
		return &qoingrpc.UserResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.UserResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.User{
			Id:       int64(usrData.Id),
			Name:     usrData.Name,
			Email:    usrData.Email,
			Path:     usrData.Path,
			Foto:     usrData.Foto,
			Username: usrData.Username,
			Password: usrData.Password,
		},
	}, nil
}
func (*UserServer) GetUser(ctx context.Context, req *qoingrpc.UserRequest) (rsp *qoingrpc.UserResponses, err error) {

	UserModel := models.User{
		Id: int32(req.User.Id),
	}

	usrData, err := UserModel.Get()

	if err != nil {
		fmt.Printf("failed to save user data %v", err)
		return &qoingrpc.UserResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.UserResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.User{
			Id:       int64(usrData.Id),
			Name:     usrData.Name,
			Email:    usrData.Email,
			Username: usrData.Username,
			Password: usrData.Password,
		},
	}, nil
}

func (*UserServer) ListUser(ctx context.Context, req *qoingrpc.UserRequest) (rsp *qoingrpc.UserListResponse, err error) {

	UserModel := models.User{
		Id:     int32(req.User.Id),
		Filter: req.User.Filter,
	}

	usrData, err := UserModel.List()

	if err != nil {
		fmt.Printf("failed to save user data %v", err)
		return &qoingrpc.UserListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	//var filter map[string]interface{}
	//json.Unmarshal([]byte(req.User.Filter), &filter)

	var loadUrl = os.Getenv("URL_IMG")

	var lUsr = make([]*qoingrpc.User, len(usrData))
	for i, val := range usrData {
		var kosong string
		if val.Foto != "" {
			kosong = loadUrl + val.Foto
		}
		lUsr[i] = &qoingrpc.User{
			Id:       int64(val.Id),
			Name:     val.Name,
			Email:    val.Email,
			Path:     val.Path,
			Foto:     val.Foto,
			Url:      kosong,
			Username: val.Username,
			Password: val.Password,
		}
	}

	return &qoingrpc.UserListResponse{
		Code:    200,
		Message: "success",
		List:    lUsr,
	}, nil
}

func (*UserServer) EditUser(ctx context.Context, req *qoingrpc.UserRequest) (rsp *qoingrpc.UserResponses, err error) {
	UserModel := models.User{
		Id:       int32(req.User.Id),
		Name:     req.User.Name,
		Email:    req.User.Email,
		Path:     req.User.Path,
		Foto:     req.User.Foto,
		Username: req.User.Username,
		//Password: global.GetMD5Hash(req.User.Password),
	}

	usrData, err := UserModel.Update()

	if err != nil {
		fmt.Printf("failed to save user data %v", err)
		return &qoingrpc.UserResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}
	fmt.Println(usrData)
	//return nil, nil

	var loadUrl = os.Getenv("URL_IMG")

	var kosong string
	if usrData.Foto != "" {
		kosong = loadUrl + usrData.Foto
	}

	return &qoingrpc.UserResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.User{
			Id:       int64(usrData.Id),
			Name:     usrData.Name,
			Email:    usrData.Email,
			Path:     usrData.Path,
			Foto:     usrData.Foto,
			Url:      kosong,
			Username: usrData.Username,
			Password: usrData.Password,
		},
	}, nil
}

func (*UserServer) DeleteUser(ctx context.Context, req *qoingrpc.UserRequest) (rsp *qoingrpc.UserResponses, err error) {
	UserModel := models.User{
		Id: int32(req.User.Id),
	}

	_, err = UserModel.Delete()

	if err != nil {
		fmt.Printf("failed to save user data %v", err)
		return &qoingrpc.UserResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.UserResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}


func (*UserServer) EditPassword(ctx context.Context, req *qoingrpc.UserRequest) (rsp *qoingrpc.UserResponses, err error) {
	UserModel := models.User{
		Id:       int32(req.User.Id),
		Password: global.GetMD5Hash(req.User.Password),
	}

	usrData, err := UserModel.UpdatePassword()

	if err != nil {
		fmt.Printf("failed to save user data %v", err)
		return &qoingrpc.UserResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}
	fmt.Println(usrData)
	//return nil, nil


	return &qoingrpc.UserResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.User{
			Id:       int64(usrData.Id),
			Password: usrData.Password,
		},
	}, nil
}