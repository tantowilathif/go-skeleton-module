package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type TestSaseServer struct {
	qoingrpc.UnimplementedTestSaseServiceServer
	qoingrpc.TestSaseListResponse
	qoingrpc.TestSaseResponses
	qoingrpc.TestSaseRequest
}

func (*TestSaseServer) TestSase(ctx context.Context, req *qoingrpc.TestSaseRequest) (rsp *qoingrpc.TestSaseListResponse, err error) {

	KartuStokModel := models.TestStokSase{
		Id:         int32(req.TestSase.Id),
		TglMulai:   req.TestSase.TanggalMulai,
		TglSelesai: req.TestSase.TanggalSelesai,
		BarangId:   int32(req.TestSase.BarangId),
		Filter:     req.TestSase.Filter,
		Offset:     req.TestSase.Offset,
		Limit:      req.TestSase.Limit,
	}

	data, err := KartuStokModel.List()
	fmt.Println(err)
	if err != nil {
		fmt.Printf("failed to save KartuStok data %v", err)
		return &qoingrpc.TestSaseListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	return &qoingrpc.TestSaseListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}
