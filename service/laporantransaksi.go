package service

import (
	"context"
	"fmt"
	"strconv"

	// "go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
	"strings"
	"time"
)

type LapTransaksiServer struct {
	qoingrpc.UnimplementedLapTransaksiServiceServer
	qoingrpc.LapTransaksiListResponse
	qoingrpc.LapTransaksiResponses
}

func (*LapTransaksiServer) LaporanTransaksi(ctx context.Context, req *qoingrpc.LapTransaksiRequest) (rsp *qoingrpc.LapTransaksiListResponse, err error) {

	TransaksiModel := models.LapFilterTransaksi{
		UserID:    int32(req.LapTransaksi.UserID),
		StartDate: req.LapTransaksi.Startdate,
		EndDate:   req.LapTransaksi.Enddate,
	}

	// fmt.Println("test")

	data, res, dataDetailTran, err := TransaksiModel.LaporanTransaksi()
	if err != nil {
		fmt.Printf("failed to save Dashboard} data %v", err)
		return &qoingrpc.LapTransaksiListResponse{
			Code:               400,
			Message:            "failed",
			LapFilterTransaksi: nil,
			LapmasterTransaksi: nil,
			RekMasterTransaksi: nil,
			LapdetailTransaksi: nil,
		}, nil
	}

	// fmt.Println(res)
	var RekMasterTransaksi = &qoingrpc.RekMasterTransaksi{
		Pemasukan:   data.Pemasukan,
		Pengeluaran: data.Pengeluaran,
		Saldo:       data.Pemasukan - data.Pengeluaran,
	}

	var LapMasterTran = make([]*qoingrpc.LapMasterTransaksi, len(dataDetailTran))
	var DetailTransaksi2 = make([]*qoingrpc.LapDetailTransaksi, len(res))

	stringToDate := func(date string) time.Time {
		t, _ := time.Parse("2006-01-02", date)
		return t
	}

	for i, val := range dataDetailTran {
		var DetailTransaksi = map[int][]*qoingrpc.LapDetailTransaksi{}
		a := strings.Split(val.Tanggal, "T")[0]
		test := stringToDate(a)
		y, m, d := test.Date()
		date := strconv.Itoa(y) + " " + m.String() + " " + strconv.Itoa(d)
		// date2 := d - 1
		for z, val2 := range res {
			ab := strings.Split(val2.Tanggal, "T")[0]
			test2 := stringToDate(ab)
			y, m, d := test2.Date()
			date := strconv.Itoa(y) + " " + m.String() + " " + strconv.Itoa(d)
			//fmt.Println(date)
			if ab == a {
				if len(res) > 0 {
					DetailTransaksi[z] = append(DetailTransaksi[z], &qoingrpc.LapDetailTransaksi{
						Id:          val2.Id,
						Tanggal:     date,
						Keterangan:  val2.Keterangan,
						Pemasukan:   float32(val2.Jumlah_masuk),
						Pengeluaran: float32(val2.Jumlah_keluar),
					})
				}
			} else {
				DetailTransaksi[z] = nil
			}
		}
		LapMasterTran[i] = &qoingrpc.LapMasterTransaksi{
			Tanggal:             date,
			Pemasukan:           float32(val.Pemasukan),
			Pengeluaran:         float32(val.Pengeluaran),
			DataDetailTransaksi: DetailTransaksi[i],
		}
	}

	for z, val2 := range res {
		a := strings.Split(val2.Tanggal, "T")[0]
		test := stringToDate(a)
		y, m, d := test.Date()
		date := strconv.Itoa(y) + " " + m.String() + " " + strconv.Itoa(d)
		DetailTransaksi2[z] = &qoingrpc.LapDetailTransaksi{
			Id:          int32(val2.Id),
			TanggalEdit: a,
			Tanggal:     date,
			Keterangan:  val2.Keterangan,
			KategoriId:  val2.KategoriId,
			Pemasukan:   float32(val2.Jumlah_masuk),
			Pengeluaran: float32(val2.Jumlah_keluar),
		}
	}

	// fmt.Println(dataDetailTran, "sapi1")
	fmt.Println(DetailTransaksi2, "sapi2")

	return &qoingrpc.LapTransaksiListResponse{
		Code:               200,
		Message:            "success",
		LapFilterTransaksi: nil,
		LapmasterTransaksi: LapMasterTran,
		RekMasterTransaksi: RekMasterTransaksi,
		LapdetailTransaksi: DetailTransaksi2,
		// RekMasterTransaksi: RekMasterTransaksi,
		// LapdetailTransaksi: DetailTransaksi,
	}, nil
}
