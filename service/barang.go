package service

import (
	"context"
	"fmt"
	"go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type BarangServer struct {
	qoingrpc.UnimplementedBarangServiceServer
	qoingrpc.BarangListResponse
	qoingrpc.BarangResponses
}

func (c *BarangServer) Barang(ctx context.Context, req *qoingrpc.BarangRequest) (rsp *qoingrpc.BarangResponses, err error) {
	BarangModel := models.Barang{
		Nama:      req.Barang.Nama,
		HargaBeli: req.Barang.HargaBeli,
		HargaJual: req.Barang.HargaJual,
		Foto: req.Barang.Foto,
		Path: req.Barang.Path,
	}
	brgData, err := BarangModel.Add()

	if err != nil {
		fmt.Printf("failed to save Barang data %v", err)
		return &qoingrpc.BarangResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.BarangResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Barang{
			Id:        int32(brgData.Id),
			Nama:      brgData.Nama,
			HargaBeli: brgData.HargaBeli,
			HargaJual: brgData.HargaJual,
		},
	}, nil
}
func (*BarangServer) GetBarang(ctx context.Context, req *qoingrpc.BarangRequest) (rsp *qoingrpc.BarangResponses, err error) {

	BarangModel := models.Barang{
		Id: int32(req.Barang.Id),
	}

	brgData, err := BarangModel.Get()

	if err != nil {
		fmt.Printf("failed to save Barang data %v", err)
		return &qoingrpc.BarangResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.BarangResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Barang{
			Id:        int32(brgData.Id),
			Nama:      brgData.Nama,
			HargaBeli: brgData.HargaBeli,
			HargaJual: brgData.HargaJual,
		},
	}, nil
}

func (*BarangServer) ListBarang(ctx context.Context, req *qoingrpc.BarangRequest) (rsp *qoingrpc.BarangListResponse, err error) {

	BarangModel := models.Barang{
		Id:     int32(req.Barang.Id),
		Filter: req.Barang.Filter,
		Offset: req.Barang.Offset,
		Limit:  req.Barang.Limit,
	}

	data, err := BarangModel.List()
	if err != nil {
		fmt.Printf("failed to save Barang data %v", err)
		return &qoingrpc.BarangListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var dataBarang = make([]*qoingrpc.Barang, len(data.List))
	for i, val := range data.List {

		/*
			MENCARI SISA STOK
		*/
		getStok := global.SisaStok{
			Barang_Id: val.Id,
		}
		sisaStok, _ := getStok.GetSisaStok()

		//fmt.Println(sisaStok)

		dataBarang[i] = &qoingrpc.Barang{
			Id:        int32(val.Id),
			Nama:      val.Nama,
			HargaBeli: val.HargaBeli,
			HargaJual: val.HargaJual,
			Stok:      sisaStok,
		}
	}

	data.List = dataBarang

	return &qoingrpc.BarangListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}

func (*BarangServer) EditBarang(ctx context.Context, req *qoingrpc.BarangRequest) (rsp *qoingrpc.BarangResponses, err error) {
	BarangModel := models.Barang{
		Id:        int32(req.Barang.Id),
		Nama:      req.Barang.Nama,
		HargaBeli: req.Barang.HargaBeli,
		HargaJual: req.Barang.HargaJual,
		Path: req.Barang.Path,
		Foto: req.Barang.Foto,
	}

	brgData, err := BarangModel.Update()

	if err != nil || brgData == nil {
		fmt.Printf("failed to save Barang data %v", err)
		return &qoingrpc.BarangResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.BarangResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Barang{
			Id:        int32(brgData.Id),
			Nama:      brgData.Nama,
			HargaBeli: brgData.HargaBeli,
			HargaJual: brgData.HargaJual,
			Path: brgData.Path,
			Foto: brgData.Foto,
		},
	}, nil
}
func (*BarangServer) DeleteBarang(ctx context.Context, req *qoingrpc.BarangRequest) (rsp *qoingrpc.BarangResponses, err error) {
	BarangModel := models.Barang{
		Id: int32(req.Barang.Id),
	}

	_, err = BarangModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Barang data %v", err)
		return &qoingrpc.BarangResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.BarangResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
