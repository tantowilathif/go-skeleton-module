package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type JabatanServer struct {
	qoingrpc.UnimplementedJabatanServiceServer
	qoingrpc.JabatanListResponse
	qoingrpc.JabatanResponses
}

func (c *JabatanServer) Jabatan(ctx context.Context, req *qoingrpc.JabatanRequest) (rsp *qoingrpc.JabatanResponses, err error) {
	JabatanModel := models.Jabatan{
		Name: req.Jabatan.Name,
	}

	jbtData, err := JabatanModel.Add()

	if err != nil {
		fmt.Printf("failed to save Jabatan data %v", err)
		return &qoingrpc.JabatanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.JabatanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Jabatan{
			Id:   int64(jbtData.Id),
			Name: jbtData.Name,
		},
	}, nil
}
func (*JabatanServer) GetJabatan(ctx context.Context, req *qoingrpc.JabatanRequest) (rsp *qoingrpc.JabatanResponses, err error) {

	JabatanModel := models.Jabatan{
		Id: int32(req.Jabatan.Id),
	}

	jbtData, err := JabatanModel.Get()

	if err != nil {
		fmt.Printf("failed to save Jabatan data %v", err)
		return &qoingrpc.JabatanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.JabatanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Jabatan{
			Id:   int64(jbtData.Id),
			Name: jbtData.Name,
		},
	}, nil
}

func (*JabatanServer) ListJabatan(ctx context.Context, req *qoingrpc.JabatanRequest) (rsp *qoingrpc.JabatanListResponse, err error) {

	JabatanModel := models.Jabatan{
		Name: req.Jabatan.Name,
	}

	jbtData, err := JabatanModel.List()

	if err != nil {
		fmt.Printf("failed to save Jabatan data %v", err)
		return &qoingrpc.JabatanListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var lUsr = make([]*qoingrpc.Jabatan, len(jbtData))
	for i, val := range jbtData {
		lUsr[i] = &qoingrpc.Jabatan{
			Id:   int64(val.Id),
			Name: val.Name,
		}
	}

	return &qoingrpc.JabatanListResponse{
		Code:    200,
		Message: "success",
		List:    lUsr,
	}, nil
}

func (*JabatanServer) EditJabatan(ctx context.Context, req *qoingrpc.JabatanRequest) (rsp *qoingrpc.JabatanResponses, err error) {

	JabatanModel := models.Jabatan{
		Name: req.Jabatan.Name,
	}

	jbtData, err := JabatanModel.Update()
	fmt.Println(JabatanModel)
	fmt.Println("alex")
	fmt.Println(jbtData)
	if err != nil {
		fmt.Printf("failed to save Jabatan data %v", err)
		return &qoingrpc.JabatanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	if jbtData != nil {
		return &qoingrpc.JabatanResponses{
			Code:    200,
			Message: "success",
			Data: &qoingrpc.Jabatan{
				Id:   int64(jbtData.Id),
				Name: jbtData.Name,
			},
		}, nil
	}
	return &qoingrpc.JabatanResponses{
		Code:    400,
		Message: "failed",
		Data:    nil,
	}, nil
}
func (*JabatanServer) DeleteJabatan(ctx context.Context, req *qoingrpc.JabatanRequest) (rsp *qoingrpc.JabatanResponses, err error) {
	JabatanModel := models.Jabatan{
		Id: int32(req.Jabatan.Id),
	}

	_, err = JabatanModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Jabatan data %v", err)
		return &qoingrpc.JabatanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.JabatanResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
