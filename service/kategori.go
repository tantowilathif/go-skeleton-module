package service

import (
	"context"
	"fmt"

	// "go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type KategoriServer struct {
	qoingrpc.UnimplementedKategoriServiceServer
	qoingrpc.KategoriListResponse
	qoingrpc.KategoriResponses
}

func (c *KategoriServer) Kategori(ctx context.Context, req *qoingrpc.KategoriRequest) (rsp *qoingrpc.KategoriResponses, err error) {
	KategoriModel := models.Kategori{
		Tipe: req.Kategori.Tipe,
		Nama: req.Kategori.Nama,
		UserID: int32(req.Kategori.UserId),
	}
	KatData, err := KategoriModel.Add()

	if err != nil {
		fmt.Printf("failed to save Kategori data %v", err)
		return &qoingrpc.KategoriResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.KategoriResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Kategori{
			Id:   int32(KatData.Id),
			Tipe: KatData.Tipe,
			Nama: KatData.Nama,
			UserId: int32(KatData.UserID),
		},
	}, nil
}
func (*KategoriServer) GetKategori(ctx context.Context, req *qoingrpc.KategoriRequest) (rsp *qoingrpc.KategoriResponses, err error) {

	KategoriModel := models.Kategori{
		Id: int32(req.Kategori.Id),
	}

	KatData, err := KategoriModel.Get()

	if err != nil {
		fmt.Printf("failed to save Kategori data %v", err)
		return &qoingrpc.KategoriResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.KategoriResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Kategori{
			Id:   int32(KatData.Id),
			Tipe: KatData.Tipe,
			Nama: KatData.Nama,
			UserId: KatData.UserID,
		},
	}, nil
}

func (*KategoriServer) ListKategori(ctx context.Context, req *qoingrpc.KategoriRequest) (rsp *qoingrpc.KategoriListResponse, err error) {

	KategoriModel := models.Kategori{
		Id:     int32(req.Kategori.Id),
		UserID:     int32(req.Kategori.UserId),
		Filter: req.Kategori.Filter,
		Offset: req.Kategori.Offset,
		Limit:  req.Kategori.Limit,
	}

	data, err := KategoriModel.List()
	if err != nil {
		fmt.Printf("failed to save Kategori data %v", err)
		return &qoingrpc.KategoriListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var dataKategori = make([]*qoingrpc.Kategori, len(data.List))
	for i, val := range data.List {

		/*
			MENCARI SISA STOK
		*/
		// getStok := global.SisaStok{
		// 	Kategori_Id: val.Id,
		// }
		// sisaStok, _ := getStok.GetSisaStok()

		//fmt.Println(sisaStok)

		dataKategori[i] = &qoingrpc.Kategori{
			Id:   int32(val.Id),
			Tipe: val.Tipe,
			Nama: val.Nama,
			UserId: val.UserId,
		}
	}

	data.List = dataKategori

	return &qoingrpc.KategoriListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}

func (*KategoriServer) ListDetailKategori(ctx context.Context, req *qoingrpc.KategoriRequest) (rsp *qoingrpc.KategoriListResponse, err error) {

	KategoriDetailModel := models.Kategori{
		Id: int32(req.Kategori.Id),
		UserID: int32(req.Kategori.UserId),
	}

	_, res, err := KategoriDetailModel.ListDetail()

	KatData, err2 := KategoriDetailModel.Get()

	if err2 != nil {
		fmt.Printf("failed to save Kategori data %v", err)
		return &qoingrpc.KategoriListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	if err != nil {
		fmt.Printf("failed to save PembelianDetail data %v", err)
		return &qoingrpc.KategoriListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}
	fmt.Println(KatData)
	fmt.Println(res)

	var data = &qoingrpc.Kategori{
		Id:   int32(KatData.Id),
		Tipe: KatData.Tipe,
		Nama: KatData.Nama,
	}
	var listData = make([]*qoingrpc.KategoriDetail, len(res))
	for i, val := range res {
		listData[i] = &qoingrpc.KategoriDetail{
			Id:         int32(val.Id),
			Tipe:       val.Tipe,
			Jumlah:     float32(val.Jumlah),
			Tanggal:    val.Tanggal,
			Keterangan: val.Keterangan,
		}
	}

	return &qoingrpc.KategoriListResponse{
		Code:         200,
		Message:      "success",
		List:         nil,
		ListKategori: data,
		ListDetail:   listData,
	}, nil

}

func (*KategoriServer) EditKategori(ctx context.Context, req *qoingrpc.KategoriRequest) (rsp *qoingrpc.KategoriResponses, err error) {
	KategoriModel := models.Kategori{
		Id:   int32(req.Kategori.Id),
		Tipe: req.Kategori.Tipe,
		Nama: req.Kategori.Nama,
	}

	KatData, err := KategoriModel.Update()

	if err != nil || KatData == nil {
		fmt.Printf("failed to save Kategori data %v", err)
		return &qoingrpc.KategoriResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.KategoriResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Kategori{
			Id:   int32(KatData.Id),
			Tipe: KatData.Tipe,
			Nama: KatData.Nama,
		},
	}, nil
}
func (*KategoriServer) DeleteKategori(ctx context.Context, req *qoingrpc.KategoriRequest) (rsp *qoingrpc.KategoriResponses, err error) {
	KategoriModel := models.Kategori{
		Id: int32(req.Kategori.Id),
	}

	_, err = KategoriModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Kategori data %v", err)
		return &qoingrpc.KategoriResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.KategoriResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
