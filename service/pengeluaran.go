package service

import (
	"context"
	"fmt"

	// "go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type PengeluaranServer struct {
	qoingrpc.UnimplementedPengeluaranServiceServer
	qoingrpc.PengeluaranListResponse
	qoingrpc.PengeluaranResponses
}

func (c *PengeluaranServer) Pengeluaran(ctx context.Context, req *qoingrpc.PengeluaranRequest) (rsp *qoingrpc.PengeluaranResponses, err error) {
	PengeluaranModel := models.Pengeluaran{
		Jumlah:   float32(req.Pengeluaran.Jumlah),
		Tanggal: req.Pengeluaran.Tanggal,
		Keterangan:  req.Pengeluaran.Keterangan,
		KategoriId: int32(req.Pengeluaran.KategoriId),
	}
	PemData, err := PengeluaranModel.Add()

	if err != nil {
		fmt.Printf("failed to save Pengeluaran data %v", err)
		return &qoingrpc.PengeluaranResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PengeluaranResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pengeluaran{
			Id:     int32(PemData.Id),
			Jumlah:   float32(PemData.Jumlah),
			Tanggal: PemData.Tanggal,
			Keterangan:  PemData.Keterangan,
			KategoriId: int32(PemData.KategoriId),
		},
	}, nil
}
func (*PengeluaranServer) GetPengeluaran(ctx context.Context, req *qoingrpc.PengeluaranRequest) (rsp *qoingrpc.PengeluaranResponses, err error) {

	PengeluaranModel := models.Pengeluaran{
		Id: int32(req.Pengeluaran.Id),
	}

	PemData, err := PengeluaranModel.Get()

	if err != nil {
		fmt.Printf("failed to save Pengeluaran data %v", err)
		return &qoingrpc.PengeluaranResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PengeluaranResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pengeluaran{
			Id:     int32(PemData.Id),
			Jumlah:   float32(PemData.Jumlah),
			Tanggal: PemData.Tanggal,
			Keterangan:  PemData.Keterangan,
			KategoriId: int32(PemData.KategoriId),
		},
	}, nil
}

func (*PengeluaranServer) ListPengeluaran(ctx context.Context, req *qoingrpc.PengeluaranRequest) (rsp *qoingrpc.PengeluaranListResponse, err error) {

	PengeluaranModel := models.Pengeluaran{
		Id:     int32(req.Pengeluaran.Id),
		Filter: req.Pengeluaran.Filter,
		Offset: req.Pengeluaran.Offset,
		Limit:  req.Pengeluaran.Limit,
	}

	data, err := PengeluaranModel.List()
	if err != nil {
		fmt.Printf("failed to save Pengeluaran data %v", err)
		return &qoingrpc.PengeluaranListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var dataPengeluaran = make([]*qoingrpc.Pengeluaran, len(data.List))
	for i, val := range data.List {

		/*
			MENCARI SISA STOK
		*/
		// getStok := global.SisaStok{
		// 	Pengeluaran_Id: val.Id,
		// }
		// sisaStok, _ := getStok.GetSisaStok()

		//fmt.Println(sisaStok)

		dataPengeluaran[i] = &qoingrpc.Pengeluaran{
			Id:     int32(val.Id),
			Jumlah:   float32(val.Jumlah),
			Tanggal: val.Tanggal,
			Keterangan:  val.Keterangan,
			KategoriId: int32(val.KategoriId),
		}
	}

	data.List = dataPengeluaran

	return &qoingrpc.PengeluaranListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}

func (*PengeluaranServer) EditPengeluaran(ctx context.Context, req *qoingrpc.PengeluaranRequest) (rsp *qoingrpc.PengeluaranResponses, err error) {
	PengeluaranModel := models.Pengeluaran{
		Id:     int32(req.Pengeluaran.Id),
		Jumlah:   float32(req.Pengeluaran.Jumlah),
		Tanggal: req.Pengeluaran.Tanggal,
		Keterangan:  req.Pengeluaran.Keterangan,
		KategoriId: int32(req.Pengeluaran.KategoriId),
	}

	PemData, err := PengeluaranModel.Update()

	if err != nil || PemData == nil {
		fmt.Printf("failed to save Pengeluaran data %v", err)
		return &qoingrpc.PengeluaranResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PengeluaranResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Pengeluaran{
			Id:     int32(PemData.Id),
			Jumlah:   float32(PemData.Jumlah),
			Tanggal: PemData.Tanggal,
			Keterangan:  PemData.Keterangan,
			KategoriId: int32(PemData.KategoriId),
		},
	}, nil
}
func (*PengeluaranServer) DeletePengeluaran(ctx context.Context, req *qoingrpc.PengeluaranRequest) (rsp *qoingrpc.PengeluaranResponses, err error) {
	PengeluaranModel := models.Pengeluaran{
		Id: int32(req.Pengeluaran.Id),
	}

	_, err = PengeluaranModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Pengeluaran data %v", err)
		return &qoingrpc.PengeluaranResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.PengeluaranResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
