package service

import (
	"context"
	"fmt"

	// "go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type TransaksiServer struct {
	qoingrpc.UnimplementedTransaksiServiceServer
	qoingrpc.TransaksiListResponse
	qoingrpc.TransaksiResponses
}

func (c *TransaksiServer) Transaksi(ctx context.Context, req *qoingrpc.TransaksiRequest) (rsp *qoingrpc.TransaksiResponses, err error) {
	TransaksiModel := models.Transaksi{
		Tipe:       req.Transaksi.Tipe,
		JumlahMasuk:     float32(req.Transaksi.JumlahMasuk),
		JumlahKeluar:     float32(req.Transaksi.JumlahKeluar),
		Tanggal:    req.Transaksi.Tanggal,
		Keterangan: req.Transaksi.Keterangan,
		KategoriId: int32(req.Transaksi.KategoriId),
		UserID: int32(req.Transaksi.UserID),
	}
	TranData, err := TransaksiModel.Add()

	if err != nil {
		fmt.Printf("failed to save Transaksi data %v", err)
		return &qoingrpc.TransaksiResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.TransaksiResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Transaksi{
			Id:         int32(TranData.Id),
			Tipe:       TranData.Tipe,
			JumlahMasuk:     float32(TranData.JumlahMasuk),
			JumlahKeluar:     float32(TranData.JumlahKeluar),
			Tanggal:    TranData.Tanggal,
			Keterangan: TranData.Keterangan,
			KategoriId: int32(TranData.KategoriId),
			UserID: int32(TranData.UserID),
		},
	}, nil
}
func (*TransaksiServer) GetTransaksi(ctx context.Context, req *qoingrpc.TransaksiRequest) (rsp *qoingrpc.TransaksiResponses, err error) {

	TransaksiModel := models.Transaksi{
		Id: int32(req.Transaksi.Id),
	}

	TranData, err := TransaksiModel.Get()

	if err != nil {
		fmt.Printf("failed to save Transaksi data %v", err)
		return &qoingrpc.TransaksiResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.TransaksiResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Transaksi{
			Id:         int32(TranData.Id),
			Tipe:       TranData.Tipe,
			JumlahMasuk:     float32(TranData.JumlahMasuk),
			JumlahKeluar:     float32(TranData.JumlahKeluar),
			Tanggal:    TranData.Tanggal,
			Keterangan: TranData.Keterangan,
			KategoriId: int32(TranData.KategoriId),
			UserID: int32(TranData.UserID),
		},
	}, nil
}

func (*TransaksiServer) ListTransaksi(ctx context.Context, req *qoingrpc.TransaksiRequest) (rsp *qoingrpc.TransaksiListResponse, err error) {

	TransaksiModel := models.Transaksi{
		Id:     int32(req.Transaksi.Id),
		UserID:     int32(req.Transaksi.UserID),
		Filter: req.Transaksi.Filter,
		Offset: req.Transaksi.Offset,
		Limit:  req.Transaksi.Limit,
	}

	data, err := TransaksiModel.List()
	if err != nil {
		fmt.Printf("failed to save Transaksi data %v", err)
		return &qoingrpc.TransaksiListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var dataTransaksi = make([]*qoingrpc.Transaksi, len(data.List))
	for i, val := range data.List {

		/*
			MENCARI SISA STOK
		*/
		// getStok := global.SisaStok{
		// 	Transaksi_Id: val.Id,
		// }
		// sisaStok, _ := getStok.GetSisaStok()

		//fmt.Println(sisaStok)

		dataTransaksi[i] = &qoingrpc.Transaksi{
			Id:         int32(val.Id),
			Tipe:       val.Tipe,
			JumlahMasuk:     float32(val.JumlahMasuk),
			JumlahKeluar:     float32(val.JumlahKeluar),
			Tanggal:    val.Tanggal,
			Keterangan: val.Keterangan,
			KategoriId: int32(val.KategoriId),
			UserID: int32(val.UserID),
		}
	}

	data.List = dataTransaksi

	return &qoingrpc.TransaksiListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}

func (*TransaksiServer) EditTransaksi(ctx context.Context, req *qoingrpc.TransaksiRequest) (rsp *qoingrpc.TransaksiResponses, err error) {
	TransaksiModel := models.Transaksi{
		Id:         int32(req.Transaksi.Id),
		Tipe:       req.Transaksi.Tipe,
		JumlahMasuk:     float32(req.Transaksi.JumlahMasuk),
		JumlahKeluar:     float32(req.Transaksi.JumlahKeluar),
		Tanggal:    req.Transaksi.Tanggal,
		Keterangan: req.Transaksi.Keterangan,
		KategoriId: int32(req.Transaksi.KategoriId),
		UserID: int32(req.Transaksi.UserID),
	}

	TranData, err := TransaksiModel.Update()

	if err != nil || TranData == nil {
		fmt.Printf("failed to save Transaksi data %v", err)
		return &qoingrpc.TransaksiResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.TransaksiResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Transaksi{
			Id:         int32(TranData.Id),
			Tipe:       TranData.Tipe,
			JumlahMasuk:     float32(TranData.JumlahMasuk),
			JumlahKeluar:     float32(TranData.JumlahKeluar),
			Tanggal:    TranData.Tanggal,
			Keterangan: TranData.Keterangan,
			KategoriId: int32(TranData.KategoriId),
			UserID: int32(TranData.UserID),
		},
	}, nil
}
func (*TransaksiServer) DeleteTransaksi(ctx context.Context, req *qoingrpc.TransaksiRequest) (rsp *qoingrpc.TransaksiResponses, err error) {
	TransaksiModel := models.Transaksi{
		Id: int32(req.Transaksi.Id),
	}

	_, err = TransaksiModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Transaksi data %v", err)
		return &qoingrpc.TransaksiResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.TransaksiResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
