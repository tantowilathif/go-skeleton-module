package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type BantuanServer struct {
	qoingrpc.UnimplementedBantuanServiceServer
	qoingrpc.BantuanListResponse
	qoingrpc.BantuanResponses
}

func (c *BantuanServer) Bantuan(ctx context.Context, req *qoingrpc.BantuanRequest) (rsp *qoingrpc.BantuanResponses, err error) {
	BantuanModel := models.Bantuan{
		Judul:       req.Bantuan.Judul,
		Keterangan:  req.Bantuan.Keterangan,
		LinkV:       req.Bantuan.LinkV,
		LinkYoutube: req.Bantuan.LinkYoutube,
	}
	bntData, err := BantuanModel.Add()

	if err != nil {
		fmt.Printf("failed to save Bantuan data %v", err)
		return &qoingrpc.BantuanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.BantuanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Bantuan{
			Id:          int64(bntData.Id),
			Judul:       bntData.Judul,
			Keterangan:  bntData.Keterangan,
			LinkV:       bntData.LinkV,
			LinkYoutube: bntData.LinkYoutube,
		},
	}, nil
}
func (*BantuanServer) GetBantuan(ctx context.Context, req *qoingrpc.BantuanRequest) (rsp *qoingrpc.BantuanResponses, err error) {

	BantuanModel := models.Bantuan{
		Id: int64(req.Bantuan.Id),
	}

	bntData, err := BantuanModel.Get()

	if err != nil {
		fmt.Printf("failed to save Bantuan data %v", err)
		return &qoingrpc.BantuanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.BantuanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Bantuan{
			Id:          int64(bntData.Id),
			Judul:       bntData.Judul,
			Keterangan:  bntData.Keterangan,
			LinkV:       bntData.LinkV,
			LinkYoutube: bntData.LinkYoutube,
		},
	}, nil
}

func (*BantuanServer) ListBantuan(ctx context.Context, req *qoingrpc.BantuanRequest) (rsp *qoingrpc.BantuanListResponse, err error) {

	BantuanModel := models.Bantuan{
		Id:     int64(req.Bantuan.Id),
		Filter: req.Bantuan.Filter,
		Offset: req.Bantuan.Offset,
		Limit:  req.Bantuan.Limit,
	}

	bntData, err := BantuanModel.List()

	if err != nil {
		fmt.Printf("failed to save Bantuan data %v", err)
		return &qoingrpc.BantuanListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	return &qoingrpc.BantuanListResponse{
		Code:    200,
		Message: "success",
		List:    bntData,
	}, nil
}

func (*BantuanServer) EditBantuan(ctx context.Context, req *qoingrpc.BantuanRequest) (rsp *qoingrpc.BantuanResponses, err error) {
	BantuanModel := models.Bantuan{
		Id:          int64(req.Bantuan.Id),
		Judul:       req.Bantuan.Judul,
		Keterangan:  req.Bantuan.Keterangan,
		LinkV:       req.Bantuan.LinkV,
		LinkYoutube: req.Bantuan.LinkYoutube,
	}

	BntData, err := BantuanModel.Update()

	if err != nil || BntData == nil {
		fmt.Printf("failed to save Bantuan data %v", err)
		return &qoingrpc.BantuanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.BantuanResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Bantuan{
			Id:          int64(BntData.Id),
			Judul:       BntData.Judul,
			Keterangan:  BntData.Keterangan,
			LinkV:       BntData.LinkV,
			LinkYoutube: BntData.LinkYoutube,
		},
	}, nil
}
func (*BantuanServer) DeleteBantuan(ctx context.Context, req *qoingrpc.BantuanRequest) (rsp *qoingrpc.BantuanResponses, err error) {
	BantuanModel := models.Bantuan{
		Id: int64(req.Bantuan.Id),
	}

	_, err = BantuanModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Bantuan data %v", err)
		return &qoingrpc.BantuanResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.BantuanResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
