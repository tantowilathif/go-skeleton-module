package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type TesRasyidServer struct {
	qoingrpc.UnimplementedTesRasyidServiceServer
	qoingrpc.TesRasyidRequest
	qoingrpc.TesRasyidResponses
	qoingrpc.TesRasyidStokReq
	qoingrpc.TesRasyidStokRes
}

func (*TesRasyidServer) TesRasyid(ctx context.Context, req *qoingrpc.TesRasyidRequest) (rsp *qoingrpc.TesRasyidResponses, err error) {
	TesRasyidModel := models.TesRasyid{
		UserId:         req.Data.UserId,
		TanggalMulai:   req.Data.TanggalMulai,
		TanggalSelesai: req.Data.TanggalSelesai,
	}

	respData, err := TesRasyidModel.Detail()
	if err != nil {
		fmt.Printf("failed to get Dashboard Grafik data %v", err)
		return &qoingrpc.TesRasyidResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.TesRasyidResponses{
		Code:    200,
		Message: "success",
		Data:    respData,
	}, nil
}

func (*TesRasyidServer) TesRasyidStokList(ctx context.Context, req *qoingrpc.TesRasyidStokReq) (rsp *qoingrpc.TesRasyidStokRes, err error) {
	TesRasyidStok := models.TesRasyidStok{
		UserId:         req.Request.UserId,
		TanggalMulai:   req.Request.TanggalMulai,
		TanggalSelesai: req.Request.TanggalSelesai,
		BarangId:       req.Request.BarangId,
	}

	respData, err := TesRasyidStok.TesRasyidStokList()
	if err != nil {
		fmt.Printf("failed to get Dashboard Grafik data %v", err)
		return &qoingrpc.TesRasyidStokRes{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.TesRasyidStokRes{
		Code:    200,
		Message: "success",
		Data:    respData,
	}, nil
}

func (*TesRasyidServer) TesRasyidJumlahList(ctx context.Context, req *qoingrpc.TesRasyidJumlahReq) (rsp *qoingrpc.TesRasyidJumlahRes, err error) {
	mod := models.TesRasyidJumlah{
		TanggalMulai:   req.Request.TanggalMulai,
		TanggalSelesai: req.Request.TanggalSelesai,
		BarangId:       req.Request.BarangId,
		Tipe:           req.Request.Tipe,
	}

	respData, err := mod.TesRasyidJumlahList()
	if err != nil {
		fmt.Printf("failed to get Dashboard Grafik data %v", err)
		return &qoingrpc.TesRasyidJumlahRes{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.TesRasyidJumlahRes{
		Code:    200,
		Message: "success",
		Data:    respData,
	}, nil
}
