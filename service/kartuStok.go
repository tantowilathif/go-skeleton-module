package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type KartuStokServer struct {
	qoingrpc.UnimplementedKartuStokServiceServer
	qoingrpc.KartuStokListResponse
	qoingrpc.KartuStokResponses
}

func (*KartuStokServer) ListKartuStok(ctx context.Context, req *qoingrpc.KartuStokRequest) (rsp *qoingrpc.KartuStokListResponse, err error) {

	KartuStokModel := models.KartuStok{
		Id:     int32(req.KartuStok.Id),
		Filter: req.KartuStok.Filter,
		Offset: req.KartuStok.Offset,
		Limit:  req.KartuStok.Limit,
	}

	data, err := KartuStokModel.List()
	if err != nil {
		fmt.Printf("failed to save KartuStok data %v", err)
		return &qoingrpc.KartuStokListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var lUsr = make([]*qoingrpc.KartuStok, len(data.List))
	for i, val := range data.List {
		lUsr[i] = &qoingrpc.KartuStok{
			Id: int32(val.Id),
		}
	}

	return &qoingrpc.KartuStokListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}
