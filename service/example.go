package service

import (
	"context"
	"fmt"
	"go-skeleton-module/rpc/qoingrpc"

	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
)

type ExampleServer struct {
	qoingrpc.UnimplementedUserServiceServer
	qoingrpc.ExampleResponses
}

func (*ExampleServer) GetExamples(ctx context.Context, in *qoingrpc.ExampleRequest) (*qoingrpc.ExampleResponses, error) {
	return &qoingrpc.ExampleResponses{
		Code:    200,
		Message: "success",
		Data:    "this is data",
	}, nil
}
func (*ExampleServer) EditExamples(ctx context.Context, in *qoingrpc.ExampleRequest) (*qoingrpc.ExampleResponses, error) {
	return &qoingrpc.ExampleResponses{
		Code:    200,
		Message: "success",
		Data:    "this is data",
	}, nil
}
func (*ExampleServer) DeleteExamples(ctx context.Context, in *qoingrpc.ExampleRequest) (*qoingrpc.ExampleResponses, error) {
	return &qoingrpc.ExampleResponses{
		Code:    200,
		Message: "success",
		Data:    "this is data",
	}, nil
}

func RabbitMQHandler(param, data interface{}) {
	qoinhelper.LoggerSuccess(fmt.Sprint(param) + fmt.Sprint(data))
}
