package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
)

type SalesServer struct {
	qoingrpc.UnimplementedSalesServiceServer
	qoingrpc.SalesListResponse
	qoingrpc.SalesResponses
}

func (c *SalesServer) Sales(ctx context.Context, req *qoingrpc.SalesRequest) (rsp *qoingrpc.SalesResponses, err error) {
	SalesModel := models.Sales{
		Nama:      req.Sales.Nama,
		NoTelp:      req.Sales.NoTelp,
		Email:      req.Sales.Email,
		Alamat:      req.Sales.Alamat,
		Detail: req.Sales.Detail,
	}
	salesData, err := SalesModel.Add()

	if err != nil {
		fmt.Printf("failed to save Sales data %v", err)
		return &qoingrpc.SalesResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.SalesResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Sales{
			Id:        int32(salesData.Id),
			Nama:      salesData.Nama,
			NoTelp: 	salesData.NoTelp,
			Email: 		salesData.Email,
			Alamat:		salesData.Alamat,
		},
	}, nil
}
func (*SalesServer) GetSales(ctx context.Context, req *qoingrpc.SalesRequest) (rsp *qoingrpc.SalesResponses, err error) {

	SalesModel := models.Sales{
		Id: int32(req.Sales.Id),
	}

	salesData, err := SalesModel.Get()

	if err != nil {
		fmt.Printf("failed to save Sales data %v", err)
		return &qoingrpc.SalesResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.SalesResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Sales{
			Id:        int32(salesData.Id),
			Nama:      salesData.Nama,
			NoTelp: 	salesData.NoTelp,
			Email: 		salesData.Email,
			Alamat:		salesData.Alamat,
		},
	}, nil
}

func (*SalesServer) ListSales(ctx context.Context, req *qoingrpc.SalesRequest) (rsp *qoingrpc.SalesListResponse, err error) {

	SalesModel := models.Sales{
		Id:     int32(req.Sales.Id),
		Filter: req.Sales.Filter,
		Offset: req.Sales.Offset,
		Limit:  req.Sales.Limit,
	}

	data, err := SalesModel.List()
	if err != nil {
		fmt.Printf("failed to save Sales data %v", err)
		return &qoingrpc.SalesListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var dataSales = make([]*qoingrpc.Sales, len(data.List))
	for i, val := range data.List {

		//fmt.Println(sisaStok)

		dataSales[i] = &qoingrpc.Sales{
			Id:        int32(val.Id),
			Nama:      val.Nama,
			NoTelp: 	val.NoTelp,
			Email:		val.Email,
			Alamat: 	val.Alamat,
		}
	}

	data.List = dataSales

	return &qoingrpc.SalesListResponse{
		Code:    200,
		Message: "success",
		List:    data,
	}, nil
}

func (*SalesServer) ListDetailSales(ctx context.Context, req *qoingrpc.SalesRequest) (rsp *qoingrpc.SalesListResponse, err error) {

	SalesDetailModel := models.Sales{
		Id: int32(req.Sales.Id),
	}

	_,res, err := SalesDetailModel.ListDetail()

	if err != nil {
		fmt.Printf("failed to save SalesDetail data %v", err)
		return &qoingrpc.SalesListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var listData = make([]*qoingrpc.SalesDetail, len(res))
	for i, val := range res {
		listData[i] = &qoingrpc.SalesDetail{
			Id:        int32(val.Id),
			SalesId: int32(val.SalesId),
			ContactPerson: val.ContactPerson,
			NoTelp: val.NoTelp,
			Bank: val.Bank,
			NamaRekening: val.NamaRekening,
			NomorRekening: val.NomorRekening,
		}
	}

	return &qoingrpc.SalesListResponse{
		Code:    200,
		Message: "success",
		List:  nil,
		ListDetail:  listData,
	}, nil

}

func (*SalesServer) EditSales(ctx context.Context, req *qoingrpc.SalesRequest) (rsp *qoingrpc.SalesResponses, err error) {
	SalesModel := models.Sales{
		Id:        int32(req.Sales.Id),
		Nama:      req.Sales.Nama,
		NoTelp:      req.Sales.NoTelp,
		Email:      req.Sales.Email,
		Alamat: 	req.Sales.Alamat,
		Detail: 	req.Sales.Detail,
	}

	salesData, err := SalesModel.Update()

	if err != nil || salesData == nil {
		fmt.Printf("failed to save Sales data %v", err)
		return &qoingrpc.SalesResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.SalesResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Sales{
			Id:        int32(salesData.Id),
			Nama:      salesData.Nama,
			NoTelp: 	salesData.NoTelp,
			Email: 		salesData.Email,
			Alamat: 	salesData.Alamat,
		},
	}, nil
}
func (*SalesServer) DeleteSales(ctx context.Context, req *qoingrpc.SalesRequest) (rsp *qoingrpc.SalesResponses, err error) {
	SalesModel := models.Sales{
		Id: int32(req.Sales.Id),
	}

	_, err = SalesModel.Delete()

	if err != nil {
		fmt.Printf("failed to save Sales data %v", err)
		return &qoingrpc.SalesResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.SalesResponses{
		Code:    200,
		Message: "success",
		Data:    nil,
	}, nil
}
