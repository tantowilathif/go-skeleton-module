package service

import (
	"context"
	"fmt"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
	"go-skeleton-module/systems"
	"strconv"
	"time"
)

type TesalexServer struct {
	qoingrpc.UnimplementedTesalexServiceServer
	qoingrpc.TesalexListResponse
	qoingrpc.TesalexResponses
}

func (*TesalexServer) LapTransaksi(ctx context.Context, req *qoingrpc.TesalexRequest) (rsp *qoingrpc.TesalexListResponse, err error) {

	TesalexModel := models.Tesalex{
		TanggalMulai:   req.Tesalex.TanggalMulai,
		TanggalSelesai: req.Tesalex.TanggalSelesai,
	}

	data, detail, err := TesalexModel.LapKeuangan()
	if err != nil {
		fmt.Printf("failed to save Tesalex} data %v", err)
		return &qoingrpc.TesalexListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var result = make([]*qoingrpc.LaporanKeuangan, len(data))
	var arrdetail = map[string][]*qoingrpc.DetailKeuangan{}
	idx := 0
	for tgl, _ := range data {
		totalMasuk := make(map[string]float32)
		totalKeluar := make(map[string]float32)
		for _, v := range detail[tgl]{
			if len(v) > 0 {
				kat := ""
				if v["kategori"] != nil{
					kat = v["kategori"].(string)
				}
				arrdetail[tgl] = append(arrdetail[tgl], &qoingrpc.DetailKeuangan{
					Tanggal: tgl,
					Pemasukan: v["pemasukan"].(float32),
					Pengeluaran: v["pengeluaran"].(float32),
					Kategori: kat,
				})
				totalMasuk[tgl] += v["pemasukan"].(float32)
				totalKeluar[tgl] += v["pengeluaran"].(float32)
			}

		}
		result[idx] = &qoingrpc.LaporanKeuangan{
			Tanggal: tgl,
			Pemasukan: totalMasuk[tgl],
			Pengeluaran: totalKeluar[tgl],
			Detail: arrdetail[tgl],
		}
		idx++
	}
	return &qoingrpc.TesalexListResponse{
		Code:        200,
		Message:     "success",
		List:        nil,
		LapKeuangan: result,
	}, nil
}
func (*TesalexServer) LaporanStok(ctx context.Context, req *qoingrpc.KartuStokAlexRequest) (rsp *qoingrpc.LaporanStokListResponse, err error) {

	KartuStokAlexModel := models.KartuStokAlex{
		BarangId:   req.StokAlex.BarangId,
		TanggalMulai:   req.StokAlex.TanggalMulai,
		TanggalSelesai: req.StokAlex.TanggalSelesai,
	}

	dataAwal, data, err := KartuStokAlexModel.LapStok()
	if err != nil {
		fmt.Printf("failed to save Tesalex} data %v", err)
		return &qoingrpc.LaporanStokListResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	var ListStokAwal = make([]*qoingrpc.SaldoAwalAlex, len(dataAwal))
	for k, v := range dataAwal {
		sisa, _ := strconv.Atoi(v["stok_awal"].(string))
		ListStokAwal[k] = &qoingrpc.SaldoAwalAlex{
			BarangId: int32(v["barang_id"].(int64)),
			StokAwal: int32(sisa),
			NamaBarang: v["nama_barang"].(string),
		}
	}

	var ArrSaldoAwal = make(map[int64]int64)
	if len(dataAwal) > 0 {
		for _ , vAwal := range dataAwal {
			sisa, _ := strconv.Atoi(vAwal["stok_awal"].(string))
			brg_id := vAwal["barang_id"].(int64)
				ArrSaldoAwal[brg_id] = int64(sisa)
		}
	}

	var arrSisa = make(map[int64]int64)
	var listData = (make([]*qoingrpc.LaporanStokAlex, len(data)))

	for key, val := range data {
		if len(val) > 0 {
			var saldoAwal int64
			barang_id := val["barang_id"].(int64)

			if _ , ok := ArrSaldoAwal[barang_id]; ok{
				saldoAwal = ArrSaldoAwal[barang_id]
				ArrSaldoAwal[barang_id] = 0
			} else{
				saldoAwal = 0
			}

			selisih := (val["jumlah_masuk"].(int64) - val["jumlah_keluar"].(int64)) + saldoAwal
			if _, ok := arrSisa[barang_id]; ok {
				arrSisa[barang_id] += selisih
			} else{
				arrSisa[barang_id] = selisih
			}

			listData[key] = &qoingrpc.LaporanStokAlex{
				Tanggal: val["tanggal"].(time.Time).Format("02 January 2006 15:04"),
				Keterangan: val["keterangan"].(string),
				JumlahMasuk: int32(val["jumlah_masuk"].(int64)),
				HargaMasuk: int32(val["harga_masuk"].(float64)),
				TotalMasuk: int32(val["jumlah_masuk"].(int64) * int64(val["harga_masuk"].(float64))),
				JumlahKeluar: int32(val["jumlah_keluar"].(int64)),
				HargaKeluar: int32(val["harga_keluar"].(float64)),
				TotalKeluar: int32(val["jumlah_keluar"].(int64) * int64(val["harga_keluar"].(float64))),
				NamaBarang: val["nama_barang"].(string),
				Sisa: int32(arrSisa[barang_id]),
			}
		}
	}

	ResultData := &qoingrpc.ListDataAlex{
		SaldoAwal: ListStokAwal,
		List: listData,
	}
	//fmt.Println(listData)
	return &qoingrpc.LaporanStokListResponse{
		Code:        200,
		Message:     "success",
		List:        ResultData,
	}, nil
}
func (*TesalexServer) LaporanRekapStokAlex(ctx context.Context, req *qoingrpc.RekapStokAlexRequest) (rsp *qoingrpc.ResultRekapStokAlexResponse, err error) {

	RekapStokAlexModel := models.RekapStokAlex{
		BarangId:   req.RekapStokAlex.BarangId,
		TanggalMulai:   req.RekapStokAlex.TanggalMulai,
		TanggalSelesai: req.RekapStokAlex.TanggalSelesai,
		Tipe: req.RekapStokAlex.Tipe,
	}

	data, detail, err := RekapStokAlexModel.LapRekapStokAlex()
	if err != nil {
		fmt.Printf("failed to save Tesalex} data %v", err)
		return &qoingrpc.ResultRekapStokAlexResponse{
			Code:    400,
			Message: "failed",
			List:    nil,
		}, nil
	}

	tgl_mulai, _ := time.Parse("2006-01-02", req.RekapStokAlex.TanggalMulai)
	tgl_selesai, _ := time.Parse("2006-01-02", req.RekapStokAlex.TanggalSelesai)
	ArrTgl := systems.IntervalDate(tgl_mulai,tgl_selesai)

	var listHeader []string
	var listData = make([]*qoingrpc.ListRekapStokAlex, len(data))
	var listDetail = make(map[int32][]int32)
	var TotalKanan = make(map[int32]int32)
	var Totalbawah = make(map[string]int32)
	var listFooter []int32

	idx := 0
	for brg_id, val := range data {

		for _, vTgl := range ArrTgl {
			tgls := vTgl.Format("2006-01-02")
			//isi detail
			if len(detail[brg_id][tgls]) > 0 {
				listDetail[brg_id] = append(listDetail[brg_id], detail[brg_id][tgls]["jumlah"].(int32))

				//total kanan
				TotalKanan[brg_id] += detail[brg_id][tgls]["jumlah"].(int32)

				//total bawah
				Totalbawah[tgls] += detail[brg_id][tgls]["jumlah"].(int32)
			} else {
				listDetail[brg_id] = append(listDetail[brg_id], 0)
				Totalbawah[tgls] += 0
			}
		}

		//push total kanan
		listDetail[brg_id] = append(listDetail[brg_id], TotalKanan[brg_id])


		listData[idx] = &qoingrpc.ListRekapStokAlex{
			BarangId: brg_id,
			NamaBarang: val["nama_barang"].(string),
			Tanggal: val["tanggal"].(time.Time).Format("2006-01-02"),
			Detail: listDetail[brg_id],
		}

		idx++
	}

	//header
	for _, date := range ArrTgl {
		listHeader = append(listHeader, date.Format("02"))
	}

	//footer
	var grandTotal int32
	for _, nilai := range Totalbawah{
		grandTotal += nilai
		listFooter = append(listFooter, nilai)
	}

	//push total kanan bawah
	listFooter = append(listFooter, grandTotal)

	ResultData := &qoingrpc.ResultRekapStok{
		Header: listHeader,
		List: listData,
		Footer: listFooter,
	}

	return &qoingrpc.ResultRekapStokAlexResponse{
		Code:   200,
		Message:"success",
		List:   ResultData,
	}, nil
}