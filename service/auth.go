package service

import (
	"context"
	"fmt"
	"go-skeleton-module/global"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc/qoingrpc"
	"os"
)

type AuthServer struct {
	qoingrpc.UnimplementedAuthServiceServer
	qoingrpc.AuthResponses
}

func (c *AuthServer) Login(ctx context.Context, req *qoingrpc.AuthRequest) (rsp *qoingrpc.AuthResponses, err error) {

	AuthModel := models.Auth{
		Username: req.Auth.Username,
		Email:    req.Auth.Email,
		Password: global.GetMD5Hash(req.Auth.Password),
	}

	authData, err := AuthModel.Get()

	if err != nil {
		fmt.Printf("failed to save user data %v", err)
		return &qoingrpc.AuthResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	var loadUrl = os.Getenv("URL_IMG")

	var kosong string
	if authData.Foto != "" {
		kosong = loadUrl + authData.Foto
	}

	return &qoingrpc.AuthResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Auth{
			Id:       authData.Id,
			Name:     authData.Name,
			Username: authData.Username,
			Url:      kosong,
			Email:    authData.Email,
		},
	}, nil

}

func (c *AuthServer) TesRasyidRefresh(ctx context.Context, req *qoingrpc.TesRasyidRefreshRequest) (rsp *qoingrpc.AuthResponses, err error) {

	AuthModel := models.Auth{
		Id: int64(req.UserId),
	}

	authData, err := AuthModel.TesRasyidGet()

	if err != nil {
		fmt.Printf("failed to get user data %v", err)
		return &qoingrpc.AuthResponses{
			Code:    400,
			Message: "failed",
			Data:    nil,
		}, nil
	}

	return &qoingrpc.AuthResponses{
		Code:    200,
		Message: "success",
		Data: &qoingrpc.Auth{
			Id:       authData.Id,
			Name:     authData.Name,
			Username: authData.Username,
			Email:    authData.Email,
		},
	}, nil

}
