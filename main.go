package main

import (
	"errors"
	qoinhelper "github.com/Qoin-Digital-Indonesia/qoingohelper"
	"github.com/joho/godotenv"
	"go-skeleton-module/models"
	"go-skeleton-module/rpc"
	"go-skeleton-module/rpc/qoinrmq"
	"os"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		qoinhelper.LoggerError(errors.New("Error while get environment file :" + err.Error()))
	}

	// Running Database Connection
	models.ModelsDb = models.Connect(1)

	forever := make(chan bool)

	// Running gRPC Server
	// this code for running gRPC server connection
	go func() {
		rpc := new(rpc.QoinRpc)
		rpc.GrpcStartServer()
	}()

	// Running RabbitMq Consumer
	// this code for running RabbitRPC server consumer
	go func() {
		rmq := new(qoinrmq.QoinRmq)
		rmq.StartConsume(os.Getenv("QOINRMQ_QUEUE"))
	}()

	//if this service handling multiple queue
	// rmq_sample_1 := new(qoinrmq.QoinRmq)
	// rmq_sample.StartConsume(os.Getenv("QUEUE_SAMPLE_SECOND"))

	// rmq_sample_2 := new(qoinrmq.QoinRmq)
	// rmq_sample_2.StartConsume(os.Getenv("QUEUE_SAMPLE_THIRD"))

	<-forever
}
