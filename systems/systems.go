package systems

import "time"

func IntervalDate(tglMulai time.Time, tglSelesai time.Time) []time.Time {
	days := tglSelesai.Sub(tglMulai).Hours() / 24
	interval := int(days)

	arrTgl := []time.Time{tglMulai}

	for i := 0; i < interval; i++ {
		tglPlus1Day := tglMulai.Add(time.Hour * 24)
		arrTgl = append(arrTgl, tglPlus1Day)
		tglMulai = tglPlus1Day
	}

	return arrTgl
}