package models

import (
	"gorm.io/gorm"
)

type Jabatan struct {
	gorm.Model
	Id    int32
	Name string
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Jabatan) Add() (jbt *Jabatan, err error) {
	tx := ModelsDb.Table("jabatan").Create(&User{
		Name: u.Name,
	}).First(&jbt)
	return jbt, tx.Error
}

// User Update Function
func (u *Jabatan) Update() (jbt *Jabatan, err error) {
	tx := ModelsDb.Table("jabatan").Where("id", u.ID).Updates(Jabatan{
		Name: u.Name,
	})

	return jbt, tx.Error
}

// User Get Function
func (u *Jabatan) Get() (jbt *Jabatan, err error) {
	tx := ModelsDb.Table("jabatan").First(&u)
	return u, tx.Error
}

// User Get Function
func (u *Jabatan) List() (jbt []*Jabatan, err error) {
	tx := ModelsDb.Table("jabatan").Limit(10).Find(&jbt)
	return jbt, tx.Error
}

// User Delete Function
func (u *Jabatan) Delete() (jbt *Jabatan, err error) {
	tx := ModelsDb.Table("jabatan").Delete(&jbt)
	return jbt, tx.Error
}
