package models

import (
	"encoding/json"
	"fmt"
	"go-skeleton-module/rpc/qoingrpc"
	"gorm.io/gorm"
	//p "github.com/Prabandham/paginator"
)

type KartuStok struct {
	gorm.Model
	Id           int32
	Keterangan   string
	ReffType     string
	ReffId       int32
	BarangId     int32
	JenisStok    string
	JumlahMasuk  int32
	HargaMasuk   float32
	JumlahKeluar int32
	HargaKeluar  float32
	CreatedAt    string
	Filter       string `gorm:"<-:false"`
	Offset       int32  `gorm:"<-:false"`
	Limit        int32  `gorm:"<-:false"`
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/
// KartuStok Get Function
func (u *KartuStok) List() (response *qoingrpc.KartuStokList, err error) {

	var data []*qoingrpc.KartuStok
	tx := ModelsDb.Table("kartu_stok")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)

	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Find(&data)
	var jmlhStok map[int]interface{}
	for i, val := range data {
		if val.ReffType == "pembelian_id" {
			jmlhStok[i] = val.JumlahMasuk
		} else {
			jmlhStok[i] = val.JumlahKeluar
		}
	}

	fmt.Println(jmlhStok)
	return &qoingrpc.KartuStokList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error

}
