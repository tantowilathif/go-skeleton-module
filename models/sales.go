package models

import (
	"encoding/json"
	"fmt"
	"strconv"

	//"go-skeleton-module/global"
	"go-skeleton-module/rpc/qoingrpc"

	"gorm.io/gorm"
)

type Sales struct {
	gorm.Model
	Id        int32
	Nama      string
	NoTelp    string
	Email 	  string
	Alamat    string
	Detail    string `gorm:"<-:false"`
	Filter    string `gorm:"<-:false"`
	Offset    int32  `gorm:"<-:false"`
	Limit     int32  `gorm:"<-:false"`
}

type  SalesDetail struct {
	Id       	int32
	SalesId     int32
	ContactPerson string
	NoTelp string
	Bank string
	NamaRekening    string
	NomorRekening    string
}

type PaginationSales struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Sales) Add() (usr *Sales, err error) {

	tx := ModelsDb.Table("sales").Create(&Sales{
		Nama:      	u.Nama,
		NoTelp: 	u.NoTelp,
		Email: 		u.Email,
		Alamat: 	u.Alamat,
	}).Last(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		for _, val := range Detail {
			fmt.Println(val)
			if u.Id > 0 {
				val["sales_id"] = u.Id
				ModelsDb.Table("sales_det").Create(val)

			}
		}
	}
	fmt.Println(Detail)
	return u, tx.Error
}

// Sales Update Function
func (u *Sales) Update() (usr *Sales, err error) {
	tx := ModelsDb.Table("sales").Where("id", u.Id).Updates(Sales{
		Nama:      u.Nama,
		NoTelp: 	u.NoTelp,
		Email: 		u.Email,
		Alamat: 	u.Alamat,
	}).First(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		//delete detail
		var query = "DELETE FROM sales_det WHERE sales_det = " + strconv.Itoa(int(u.Id));
		ModelsDb.Exec(query)
		//insert detail
		for _, val := range Detail {
			val["sales_det"] = u.Id
			ModelsDb.Table("sales_det").Create(val)

		}
	}

	return u, tx.Error
}

// Sales Get Function
func (u *Sales) Get() (usr *Sales, err error) {
	tx := ModelsDb.Table("sales").First(&u)
	return u, tx.Error
}

// Sales Get Function
func (u *Sales) List() (response *qoingrpc.SalesList, err error) {

	var data []*qoingrpc.Sales
	tx := ModelsDb.Table("sales")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.SalesList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error

}

func (u *Sales) ListDetail() (data []*Sales, dataDetail []*SalesDetail, err error) {
	tx := ModelsDb.Select("*").Table("sales_det").Where("sales_id", u.Id).Find(&dataDetail)

	return data, dataDetail, tx.Error
}

// Sales Delete Function
func (u *Sales) Delete() (usr *Sales, err error) {
	tx := ModelsDb.Table("sales").Delete(&u)
	return u, tx.Error
}
