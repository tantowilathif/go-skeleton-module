package models

import (
	"encoding/json"
	"fmt"

	//"go-skeleton-module/global"
	"go-skeleton-module/rpc/qoingrpc"
	// "strconv"

	"gorm.io/gorm"
)

type Transaksi struct {
	gorm.Model
	Id         int32
	Tipe       string
	JumlahMasuk     float32
	JumlahKeluar     float32
	Tanggal    string
	Keterangan string
	KategoriId int32
	UserID int32
	Filter     string `gorm:"<-:false"`
	Offset     int32  `gorm:"<-:false"`
	Limit      int32  `gorm:"<-:false"`
}

type PaginationTransaksi struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Transaksi) Add() (usr *Transaksi, err error) {

	tx := ModelsDb.Table("transaksi_keuangan").Create(&Transaksi{
		Tipe:       u.Tipe,
		JumlahMasuk:     float32(u.JumlahMasuk),
		JumlahKeluar:     float32(u.JumlahKeluar),
		Tanggal:    u.Tanggal,
		Keterangan: u.Keterangan,
		KategoriId: int32(u.KategoriId),
		UserID: int32(u.UserID),
	}).Last(&u)

	return u, tx.Error
}

// Transaksi Update Function
func (u *Transaksi) Update() (usr *Transaksi, err error) {
	tx := ModelsDb.Table("transaksi_keuangan").Where("id", u.Id).Updates(Transaksi{
		Tipe:       u.Tipe,
		JumlahMasuk:     float32(u.JumlahMasuk),
		JumlahKeluar:     float32(u.JumlahKeluar),
		Tanggal:    u.Tanggal,
		Keterangan: u.Keterangan,
		KategoriId: int32(u.KategoriId),
		UserID: int32(u.UserID),
	}).First(&u)

	return u, tx.Error
}

// Transaksi Get Function
func (u *Transaksi) Get() (usr *Transaksi, err error) {
	tx := ModelsDb.Table("transaksi_keuangan").First(&u)
	return u, tx.Error
}

// Transaksi Get Function
func (u *Transaksi) List() (response *qoingrpc.TransaksiList, err error) {

	var data []*qoingrpc.Transaksi
	// var data2 []*qoingrpc.Transaksi
	tx := ModelsDb.Table("transaksi_keuangan")

	// lastWeek := "2021-10-01"
	// today := "2021-10-07"
	// tx2 := ModelsDb.Table("view_grafik").Where("tanggal BETWEEN ? AND ?", lastWeek, today).Find(&data2)

	// // var dataArray = map[string]string{"tanggal":"nil","data":[]string{}}
	// // var dataArray []map[string]interface{}
	// 	// fmt.Println(u)]
	// fmt.Println(tx2)
	// fmt.Println("test")
	// // fmt.Println(data2)
	// for _, val := range data2 {
	// 	// fmt.Println(*val)
	// 	// dataArray := map[string]interface{}{
	// 	// 	"tanggal" : val.Tanggal,
	// 	// 	"masuk" : val.Jumlah,
	// 	// 	"keluar" : val.Jumlah,
	// 	// }
	// 	// dataArray["data"] = make(map[string]string)

	// 	fmt.Println(*val.pengeluaran)
	// }

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	tx.Where("user_id = ?", u.UserID)
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.TransaksiList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error
}

// Transaksi Delete Function
func (u *Transaksi) Delete() (usr *Transaksi, err error) {
	tx := ModelsDb.Table("transaksi_keuangan").Delete(&u)
	return u, tx.Error
}
