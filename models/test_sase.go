package models

import (
	"go-skeleton-module/rpc/qoingrpc"
	"gorm.io/gorm"
	"strconv"
	"time"
	//p "github.com/Prabandham/paginator"
)

type TestStokSase struct {
	gorm.Model
	Id           int32
	Barang       string
	Keterangan   string
	ReffType     string
	ReffId       int32
	BarangId     int32
	JenisStok    string
	JumlahMasuk  int32
	HargaMasuk   float32
	JumlahKeluar int32
	HargaKeluar  float32
	SisaStok     int32
	TglMulai     string
	TglSelesai   string
	CreatedAt    string
	Filter       string `gorm:"<-:false"`
	Offset       int32  `gorm:"<-:false"`
	Limit        int32  `gorm:"<-:false"`
}

type DataBarangSblmnya struct {
	BarangId int32
	Barang   string
	StokAwal int32
}

type DetailBarangStok struct {
	BarangId int32
	Barang   string
}

type MasterTipeStok struct {
	Tipe string
}

type MasterBarangStok2 struct {
	Tanggal     string
	TotalMasuk  int32
	TotalKeluar int32
	BarangId    int32
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/
// KartuStok Get Function
func (u *TestStokSase) List() (response *qoingrpc.TestSaseList, err error) {

	//var data []*qoingrpc.TestSase
	var data []map[string]interface{}
	var dataStokSblmnya []*qoingrpc.DataBarangSblmnya
	//var MasterStokBarang []*MasterBarangStok2
	DataStok := ModelsDb.Table("kartu_stok")
	DataStok.Select("kartu_stok.barang_id, barang.nama as barang, sum( jumlah_masuk  - jumlah_keluar) as stok_awal")
	DataStok.Joins("JOIN barang ON kartu_stok.barang_id = barang.id")
	DataStok.Where("DATE(kartu_stok.created_at) < ?", u.TglMulai)
	if u.BarangId != 0 {
		DataStok.Where("kartu_stok.barang_id = ?", u.BarangId)
	}
	DataStok.Group("kartu_stok.barang_id")
	DataStok.Find(&dataStokSblmnya)

	//tx := ModelsDb.Table("kartu_stok")
	//tx.Select("kartu_stok.*, barang.nama as barang")
	//tx.Joins("JOIN barang ON kartu_stok.barang_id = barang.id")
	//if u.TglMulai != "" {
	//	tx.Where("DATE(kartu_stok.created_at) BETWEEN ? AND ?", u.TglMulai, u.TglSelesai)
	//}
	//if u.BarangId != 0 {
	//	tx.Where("kartu_stok.barang_id = ?", u.BarangId)
	//}
	//var count int64
	//tx.Order("DATE(kartu_stok.created_at) asc")
	//tx.Count(&count)
	//tx.Find(&data)

	query := "" +
		"DATE(kartu_stok.created_at) tanggal," +
		"CASE WHEN 'jenis_stok' = 'masuk' THEN SUM(kartu_stok.jumlah_masuk) ELSE SUM(kartu_stok.jumlah_keluar) END jumlah," +
		"kartu_stok.barang_id," +
		"barang.nama as barang"
	//fmt.Println(query, "al")
	tx := ModelsDb.Select(query)
	tx.Table("kartu_stok")
	tx.Joins("LEFT JOIN barang ON kartu_stok.barang_id = barang.id")
	tx.Where("DATE(kartu_stok.created_at) BETWEEN ? AND ?", u.TglMulai, u.TglSelesai)

	if u.BarangId != 0 {
		tx.Where("kartu_stok.barang_id IN (?)", u.BarangId)
	}

	tx.Where("kartu_stok.deleted_at IS NULL")
	tx.Order("barang.nama")
	tx.Group("kartu_stok.barang_id, DATE(kartu_stok.created_at)")
	tx.Find(&data)

	dtIntrval := GetDateInterval(Strtotime(u.TglMulai), Strtotime(u.TglSelesai))
	allDt := []string{}
	//var MstrBrg = make([]*qoingrpc.MasterBarangStok, 1)
	var dBrg = make(map[int32]map[string]map[string]interface{})
	var mBrg = make(map[int32]map[string]interface{})

	for _, val := range data {
		b_id := int32(val["barang_id"].(int64))
		if dBrg[b_id] == nil {
			dBrg[b_id] = make(map[string]map[string]interface{}, len(allDt))
		}

		tgl := val["tanggal"].(time.Time).Format("2006-01-02")
		jml, _ := strconv.Atoi(val["jumlah"].(string))
		dBrg[b_id][tgl] = map[string]interface{}{
			"jumlah": int32(jml),
		}

		if _, tot := mBrg[b_id]; tot == false {
			mBrg[b_id] = map[string]interface{}{
				"nama_barang": val["nama_barang"].(string),
			}
		}
	}

	var lsDet = make(map[int32][]int32)
	var TtlKanan = make(map[int32]int32)
	var Ttlbawah = make(map[string]int32)
	index := 0
	var StkBrg = make([]*qoingrpc.DetailBarangStok, len(mBrg))
	var MstrBrg *qoingrpc.MasterBarangStok
	for b_id, val := range mBrg {
		for _, arrTgl := range dtIntrval {
			tgls := arrTgl.Format("2006-01-02")
			if len(dBrg[b_id][tgls]) > 0 {
				lsDet[b_id] = append(lsDet[b_id], dBrg[b_id][tgls]["jumlah"].(int32))

				//total kanan
				TtlKanan[b_id] += dBrg[b_id][tgls]["jumlah"].(int32)

				//total bawah
				Ttlbawah[tgls] += dBrg[b_id][tgls]["jumlah"].(int32)
			} else {
				lsDet[b_id] = append(lsDet[b_id], 0)
				Ttlbawah[tgls] += 0
			}
		}
		StkBrg[index] = &qoingrpc.DetailBarangStok{
			Id:     int32(val["barang_id"].(int64)),
			Barang: val["barang"].(string),
			Detail: nil,
		}
		index++
	}

	for _, dt := range dtIntrval {
		allDt = append(allDt, Timetoday(dt))
	}

	var listTotal []int32
	var grandTotal int32
	for _, nilai := range Ttlbawah {
		grandTotal += nilai
		listTotal = append(listTotal, nilai)
	}
	listTotal = append(listTotal, grandTotal)

	MstrBrg = &qoingrpc.MasterBarangStok{
		Tanggal: allDt,
		List:    StkBrg,
		Total:   nil,
	}
	//var jmlhStok = make(map[int32]int32, len(data))
	//for _, val := range dataStokSblmnya {
	//	jmlhStok[val.BarangId] = val.StokAwal
	//}
	//for _, val := range data {
	//	jmlhStok[val.BarangId] += val.JumlahMasuk
	//	jmlhStok[val.BarangId] -= val.JumlahKeluar
	//	val.SisaStok = jmlhStok[val.BarangId]
	//}

	return &qoingrpc.TestSaseList{
		Limit:   u.Limit,
		Offset:  u.Offset,
		LapStok: MstrBrg,
		//DataStok:   dataStokSblmnya,
		//TotalItems: int32(count),
		//List:       data,
	}, tx.Error

}
