package models

import (
	"encoding/json"
	"fmt"
	"go-skeleton-module/rpc/qoingrpc"
	"strconv"
	"time"

	"gorm.io/gorm"
)

type Pembelian struct {
	gorm.Model
	Id       int32
	Tanggal  string
	Supplier string
	Detail   string `gorm:"<-:false"`
	Filter   string `gorm:"<-:false"`
	Offset   int32  `gorm:"<-:false"`
	Limit    int32  `gorm:"<-:false"`
}
type PembelianDetail struct {
	Id          int32
	PembelianId int32
	BarangId    int32
	Nama        string
	Harga       float32
	Jumlah      int32
	Total       float32
}
type PaginationPembelian struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Pembelian) Add() (data *Pembelian, err error) {
	tx := ModelsDb.Table("pembelian").Create(&Pembelian{
		Tanggal:  u.Tanggal,
		Supplier: u.Supplier,
	}).Last(&u)
	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		for _, val := range Detail {
			if u.Id > 0 {
				val["pembelian_id"] = u.Id
				ModelsDb.Table("pembelian_det").Create(val)

				dataStok := map[string]interface{}{
					"keterangan":   "Pembelian " + val["nama"].(string),
					"reff_type":    "pembelian_id",
					"reff_id":      int32(u.Id),
					"jenis_stok":   "masuk",
					"jumlah_masuk": val["jumlah"],
					"harga_masuk":  val["harga"],
					"created_at":   time.Now().Format("20060102150405"),
				}

				ModelsDb.Table("kartu_stok").Create(dataStok)
			}
		}
	}
	return u, tx.Error
}

// Pembelian Update Function
func (u *Pembelian) Update() (data *Pembelian, err error) {
	tx := ModelsDb.Table("pembelian").Where("id", u.Id).Updates(Pembelian{
		Tanggal:  u.Tanggal,
		Supplier: u.Supplier,
	}).First(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		//delete detail
		var query = "DELETE FROM pembelian_det WHERE pembelian_id = " + strconv.Itoa(int(u.Id))
		ModelsDb.Exec(query)
		var queryStok = "DELETE FROM kartu_stok WHERE reff_type = 'pembelian_id' AND reff_id = " + strconv.Itoa(int(u.Id))
		ModelsDb.Exec(queryStok)

		//insert detail
		for _, val := range Detail {
			val["pembelian_id"] = u.Id
			ModelsDb.Table("pembelian_det").Create(val)

			dataStok := map[string]interface{}{
				"keterangan":   "Pembelian " + val["nama"].(string),
				"reff_type":    "pembelian_id",
				"reff_id":      int32(u.Id),
				"jenis_stok":   "masuk",
				"jumlah_masuk": val["jumlah"],
				"harga_masuk":  val["harga"],
				"created_at":   time.Now().Format("20060102150405"),
			}

			ModelsDb.Table("kartu_stok").Create(dataStok)
		}
	}

	return u, tx.Error
}

// Pembelian Get Function
func (u *Pembelian) Get() (data *Pembelian, err error) {
	tx := ModelsDb.Table("pembelian").First(&u)
	return u, tx.Error
}

// Pembelian Get Function
func (u *Pembelian) List() (response *qoingrpc.PembelianList, err error) {

	var data []*qoingrpc.Pembelian
	tx := ModelsDb.Table("pembelian")
	tx.Where("deleted_at IS NULL")
	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.PembelianList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error

}

func (u *Pembelian) ListDetail() (data []*Pembelian, dataDetail []*PembelianDetail, err error) {
	tx := ModelsDb.Select("*").Table("pembelian_det").Where("pembelian_id", u.Id).Find(&dataDetail)

	return data, dataDetail, tx.Error
}

// Pembelian Delete Function
func (u *Pembelian) Delete() (data *Pembelian, err error) {
	var queryStok = "DELETE FROM kartu_stok WHERE reff_type = 'pembelian_id' AND reff_id = " + strconv.Itoa(int(u.Id))
	ModelsDb.Exec(queryStok)

	tx := ModelsDb.Table("pembelian").Delete(&u)

	return u, tx.Error
}
