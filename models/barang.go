package models

import (
	"encoding/json"
	"fmt"
	"os"

	//"go-skeleton-module/global"
	"go-skeleton-module/rpc/qoingrpc"
	"gorm.io/gorm"
)

type Barang struct {
	gorm.Model
	Id        int32
	Nama      string
	HargaJual float32
	HargaBeli float32
	Foto 	 string
	Path 	 string
	Filter    string `gorm:"<-:false"`
	Offset    int32  `gorm:"<-:false"`
	Limit     int32  `gorm:"<-:false"`
}

type Pagination struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Barang) Add() (usr *Barang, err error) {

	tx := ModelsDb.Table("barang").Create(&Barang{
		Nama:      u.Nama,
		HargaBeli: u.HargaBeli,
		HargaJual: u.HargaJual,
		Foto: u.Foto,
		Path: u.Path,
	}).Last(&u)
	return u, tx.Error
}

// Barang Update Function
func (u *Barang) Update() (usr *Barang, err error) {
	//unlink
	ModelsDb.Table("barang").Where("id", u.Id).First(&usr)
	os.Remove(usr.Path + usr.Foto)
	//end unlink

	tx := ModelsDb.Table("barang").Where("id", u.Id).Updates(Barang{
		Nama:      u.Nama,
		HargaBeli: u.HargaBeli,
		HargaJual: u.HargaJual,
		Foto: u.Foto,
		Path: u.Path,
	}).First(&u)
	return u, tx.Error
}

// Barang Get Function
func (u *Barang) Get() (usr *Barang, err error) {
	tx := ModelsDb.Table("barang").First(&u)
	return u, tx.Error
}

// Barang Get Function
func (u *Barang) List() (response *qoingrpc.BarangList, err error) {
	var data []*qoingrpc.Barang
	tx := ModelsDb.Table("barang")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.BarangList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error

}

// Barang Delete Function
func (u *Barang) Delete() (usr *Barang, err error) {
	tx := ModelsDb.Table("barang").Delete(&u)
	return u, tx.Error
}
