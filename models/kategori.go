package models

import (
	"encoding/json"
	"fmt"

	//"go-skeleton-module/global"
	"go-skeleton-module/rpc/qoingrpc"
	// "strconv"

	"gorm.io/gorm"
)

type Kategori struct {
	gorm.Model
	Id     int32
	Tipe   string
	Nama   string
	UserID int32
	Detail string `gorm:"<-:false"`
	Filter string `gorm:"<-:false"`
	Offset int32  `gorm:"<-:false"`
	Limit  int32  `gorm:"<-:false"`
}

type KategoriDetail struct {
	Id         int32
	Tipe       string
	Jumlah     float32
	Tanggal    string
	Keterangan string
}

type PaginationKategori struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Kategori) Add() (usr *Kategori, err error) {

	tx := ModelsDb.Table("kategori").Create(&Kategori{
		Tipe: u.Tipe,
		Nama: u.Nama,
		UserID: u.UserID,
	}).Last(&u)

	return u, tx.Error
}

// Kategori Update Function
func (u *Kategori) Update() (usr *Kategori, err error) {
	tx := ModelsDb.Table("kategori").Where("id", u.Id).Updates(Kategori{
		Tipe: u.Tipe,
		Nama: u.Nama,
	}).First(&u)

	return u, tx.Error
}

// Kategori Get Function
func (u *Kategori) Get() (usr *Kategori, err error) {
	tx := ModelsDb.Table("kategori").First(&u)
	return u, tx.Error
}

// Kategori Get Function
func (u *Kategori) List() (response *qoingrpc.KategoriList, err error) {

	var data []*qoingrpc.Kategori
	tx := ModelsDb.Table("kategori")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	tx.Where("user_id = ? OR user_id = 0", u.UserID)
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.KategoriList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error
}

func (u *Kategori) ListDetail() (data []*Kategori, dataDetail []*KategoriDetail, err error) {
	tx := ModelsDb.Table("kategori")
	tx.Joins("inner join transaksi_keuangan on kategori.id = transaksi_keuangan.kategori_id")
	tx.Joins("inner join transaksi_keuangan on users.id = transaksi_keuangan.user_id")
	tx.Select("*")
	tx.Where("transaksi_keuangan.kategori_id", u.Id)
	tx.Where("transaksi_keuangan.user_id", u.UserID)
	tx.Find(&dataDetail)

	fmt.Println(tx)
	for _, val := range dataDetail {
		fmt.Println(*val)
	}
	return data, dataDetail, tx.Error
}

// Kategori Delete Function
func (u *Kategori) Delete() (usr *Kategori, err error) {
	tx := ModelsDb.Table("kategori").Delete(&u)
	return u, tx.Error
}
