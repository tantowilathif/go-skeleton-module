package models

import (
	"encoding/json"
	"fmt"
	"os"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Id       int32
	Name     string
	Email    string
	Path     string
	Foto     string
	Username string
	Password string
	Offset   int32  `gorm:"<-:false"`
	Limit    int32  `gorm:"<-:false"`
	Filter   string `gorm:"<-:false"`
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *User) Add() (usr *User, err error) {
	tx := ModelsDb.Table("users").Create(&User{
		Name:     u.Name,
		Email:    u.Email,
		Path:     u.Path,
		Foto:     u.Foto,
		Username: u.Username,
		Password: u.Password,
	}).Last(&usr)
	return usr, tx.Error
}

// User Update Function
func (u *User) Update() (usr *User, err error) {
	//unlink
	ModelsDb.Table("users").Where("id", u.Id).First(&usr)
	os.Remove(usr.Path + usr.Foto)
	//end unlink

	tx := ModelsDb.Table("users").Where("id", u.Id).Updates(User{
		Name:     u.Name,
		Email:    u.Email,
		Path:     u.Path,
		Foto:     u.Foto,
		Username: u.Username,
	}).First(&u)

	return u, tx.Error
}

// Update Password 
func (u *User) UpdatePassword()(usr *User, err error){

	tx := ModelsDb.Table("users").Where("id", u.Id).Updates(User{
		Password: u.Password,
	}).First(&u)

	return u, tx.Error
}

// User Get Function
func (u *User) Get() (usr *User, err error) {
	tx := ModelsDb.Table("users").First(&u)
	return u, tx.Error
}

// User Get Function
func (u *User) List() (usr []*User, err error) {
	tx := ModelsDb.Table("users").Where("deleted_at is NULL")
	
	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)

	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	tx.Find(&usr)

	return usr, tx.Error
}

// User Delete Function
func (u *User) Delete() (usr *User, err error) {
	tx := ModelsDb.Table("users").Where("id=?", u.Id).Delete(&usr)
	return usr, tx.Error
}
