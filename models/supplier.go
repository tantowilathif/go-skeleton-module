package models

import (
	"encoding/json"
	"fmt"
	"go-skeleton-module/rpc/qoingrpc"
	"strconv"

	"gorm.io/gorm"
)

type Supplier struct {
	gorm.Model
	Id     int64
	Nama   string
	NoTelp string
	Email  string
	Alamat string
	Detail string `gorm:"<-:false"`
	Filter string `gorm:"<-:false"`
	Offset int32  `gorm:"<-:false"`
	Limit  int32  `gorm:"<-:false"`
}

type SupplierDetail struct {
	Id            int32
	SupplierId    int32
	ContactPerson string
	NoTelp        string
	Bank          string
	NamaRekening  string
	NomorRekening string
}

type PaginationSupplier struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&Supplier{}).argument(...)
*/

// Supplier Add Function
func (u *Supplier) Add() (usr *Supplier, err error) {

	tx := ModelsDb.Table("supplier").Create(&Supplier{
		Nama:   u.Nama,
		NoTelp: u.NoTelp,
		Email:  u.Email,
		Alamat: u.Alamat,
	}).Last(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		for _, val := range Detail {
			if u.Id > 0 {
				val["supplier_id"] = u.Id
				ModelsDb.Table("supplier_det").Create(val)
			}
		}
	}

	return u, tx.Error
}

// Supplier Update Function
func (u *Supplier) Update() (usr *Supplier, err error) {
	tx := ModelsDb.Table("supplier").Where("id", u.Id).Updates(Supplier{
		Nama:   u.Nama,
		NoTelp: u.NoTelp,
		Email:  u.Email,
		Alamat: u.Alamat,
	}).First(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		//delete detail
		var query = "DELETE FROM supplier_det WHERE supplier_id = " + strconv.Itoa(int(u.Id))
		ModelsDb.Exec(query)

		//insert detail
		for _, val := range Detail {
			val["supplier_id"] = u.Id
			ModelsDb.Table("supplier_det").Create(val)
		}
	}

	return u, tx.Error
}

// Supplier Get Function
func (u *Supplier) Get() (usr *Supplier, err error) {
	tx := ModelsDb.Table("supplier").First(&u)
	return u, tx.Error
}

// Supplier Get Function
func (u *Supplier) List() (response *qoingrpc.SupplierList, err error) {

	var data []*qoingrpc.Supplier
	tx := ModelsDb.Table("supplier")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.SupplierList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error

}

// Supplier List Detail
func (u *Supplier) ListDetail() (data []*Supplier, dataDetail []*SupplierDetail, err error) {
	tx := ModelsDb.Select("*").Table("supplier_det").Where("supplier_id", u.Id).Find(&dataDetail)

	return data, dataDetail, tx.Error
}

// Supplier Delete Function
func (u *Supplier) Delete() (usr *Supplier, err error) {
	tx := ModelsDb.Table("supplier").Delete(&u)
	return u, tx.Error
}
