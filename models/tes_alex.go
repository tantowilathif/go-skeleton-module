package models

import (
	"gorm.io/gorm"
	"strconv"
	"time"

	//p "github.com/Prabandham/paginator"
)

type Tesalex struct {
	gorm.Model
	Id             int32
	TanggalMulai   string
	TanggalSelesai string
}

type KartuStokAlex struct {
	BarangId       int32
	TanggalMulai   string
	TanggalSelesai string
}
type RekapStokAlex struct {
	BarangId       string
	TanggalMulai   string
	TanggalSelesai string
	Tipe string
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/
// Tesalex}} Get Function
func (u *Tesalex) LapKeuangan() (res map[string]map[string]interface{},resdet map[string][]map[string]interface{}, err error) {

	var data []map[string]interface{}
	query := "" +
		"DATE(transaksi_keuangan.tanggal) tanggal," +
		"transaksi_keuangan.jumlah_masuk pemasukan," +
		"transaksi_keuangan.jumlah_keluar pengeluaran," +
		"transaksi_keuangan.keterangan," +
		"kategori.nama kategori"
	tx := ModelsDb.Select(query).Table("transaksi_keuangan").Joins("left join kategori on transaksi_keuangan.kategori_id = kategori.id")
	tx.Where("DATE(transaksi_keuangan.tanggal) BETWEEN ? AND ?", u.TanggalMulai, u.TanggalSelesai)
	tx.Where("transaksi_keuangan.deleted_at IS NULL")
	tx.Order("DATE(transaksi_keuangan.tanggal)")
	tx.Find(&data)

	det := make(map[string][]map[string]interface{})
	m := make(map[string]map[string]interface{})
	if len(data) > 0 {
		for i, val := range data {
			tgl := val["tanggal"].(time.Time).Format("2006-01-02")

			if det[tgl] == nil {
				det[tgl] = make([]map[string]interface{}, len(data))
			}

			det[tgl][i] = map[string]interface{}{
				"tanggal" : tgl,
				"pemasukan" : float32(val["pemasukan"].(float64)),
				"pengeluaran" : float32(val["pengeluaran"].(float64)),
				"kategori" : val["kategori"],
			}

			if _, tot := m[tgl]; tot == false  {
				m[tgl] = map[string]interface{}{
					"tanggal" : tgl,
					"pemasukan" : 0,
					"pengeluaran" : 0,
				}
			}

		}

		return m, det, err
	}


	return nil, nil, err
}
func (u *KartuStokAlex) LapStok() (dataAwal []map[string]interface{}, data []map[string]interface{}, err error) {

	queryAwal := "" +
		"SUM(kartu_stok.jumlah_masuk - kartu_stok.jumlah_keluar) stok_awal," +
		"kartu_stok.barang_id," +
		"barang.nama nama_barang"
	txAwal := ModelsDb.Select(queryAwal).Table("kartu_stok").Joins("LEFT JOIN barang ON kartu_stok.barang_id = barang.id")
	txAwal.Where("DATE(kartu_stok.created_at) < ?", u.TanggalMulai)

	if int32(u.BarangId) > 0 {
		txAwal.Where("kartu_stok.barang_id", int32(u.BarangId))
	}

	txAwal.Where("kartu_stok.deleted_at IS NULL")
	txAwal.Group("kartu_stok.barang_id")
	txAwal.Find(&dataAwal)

	query := "" +
		"kartu_stok.created_at tanggal," +
		"kartu_stok.jumlah_masuk," +
		"kartu_stok.harga_masuk," +
		"kartu_stok.jumlah_keluar," +
		"kartu_stok.harga_keluar," +
		"kartu_stok.keterangan," +
		"kartu_stok.barang_id," +
		"barang.nama nama_barang"
	tx := ModelsDb.Select(query).Table("kartu_stok").Joins("LEFT JOIN barang ON kartu_stok.barang_id = barang.id")
	tx.Where("DATE(kartu_stok.created_at) BETWEEN ? AND ?", u.TanggalMulai, u.TanggalSelesai)

	if int32(u.BarangId) > 0 {
		tx.Where("kartu_stok.barang_id", int32(u.BarangId))
	}

	tx.Where("kartu_stok.deleted_at IS NULL")
	tx.Order("kartu_stok.created_at")
	tx.Find(&data)

	return dataAwal, data, err
}
func (u *RekapStokAlex) LapRekapStokAlex() (res map[int32]map[string]interface{}, resdet map[int32]map[string]map[string]interface{}, err error) {
	var data []map[string]interface{}
	query := "" +
		"DATE(kartu_stok.created_at) tanggal," +
		"CASE WHEN '" + u.Tipe + "' = 'masuk' THEN SUM(kartu_stok.jumlah_masuk) ELSE SUM(kartu_stok.jumlah_keluar) END jumlah," +
		"kartu_stok.barang_id," +
		"barang.nama nama_barang"
	//fmt.Println(query, "al")
	tx := ModelsDb.Select(query).Table("kartu_stok").Joins("LEFT JOIN barang ON kartu_stok.barang_id = barang.id")
	tx.Where("DATE(kartu_stok.created_at) BETWEEN ? AND ?", u.TanggalMulai, u.TanggalSelesai)

	if u.BarangId != "" {
		tx.Where("kartu_stok.barang_id IN (?)", u.BarangId)
	}

	tx.Where("kartu_stok.deleted_at IS NULL")
	tx.Order("barang.nama")
	tx.Group("kartu_stok.barang_id, DATE(kartu_stok.created_at)")
	tx.Find(&data)

	det := make(map[int32]map[string]map[string]interface{})
	m := make(map[int32]map[string]interface{})
	if len(data) > 0 {
		for _, val := range data {
			tgl := val["tanggal"].(time.Time).Format("2006-01-02")
			brg_id := int32(val["barang_id"].(int64))
			jml, _ := strconv.Atoi(val["jumlah"].(string))
			if det[brg_id] == nil {
				det[brg_id] = make(map[string]map[string]interface{})
			}

			det[brg_id][tgl] = map[string]interface{}{
				"nama_barang" : val["nama_barang"].(string),
				"jumlah" : int32(jml),
				"tanggal" : tgl,
			}

			if _, tot := m[brg_id]; tot == false  {
				m[brg_id] = map[string]interface{}{
					"nama_barang" : val["nama_barang"].(string),
					"tanggal" : val["tanggal"].(time.Time),
				}
			}
		}
	}

	return m, det, err
}
