package models

import (
	"encoding/json"
	"fmt"
	"go-skeleton-module/rpc/qoingrpc"
	"gorm.io/gorm"
	"strconv"
	"time"
)

type Penjualan struct {
	gorm.Model
	Id       int32
	Tanggal  string
	TotalHarga float32
	TotalBayar float32
	Kembalian float32
	StatusLunas string
	Detail string `gorm:"<-:false"`
	Filter    string `gorm:"<-:false"`
	Offset    int32  `gorm:"<-:false"`
	Limit     int32  `gorm:"<-:false"`
}
type  PenjualanDetail struct {
	Id       int32
	PenjualanId  int32
	BarangId int32
	Nama string
	Harga float32
	Jumlah int32
	Total float32
}
type PaginationPenjualan struct {
	Limit  int
	Offset int
	Count  int
}
// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Penjualan) Add() (data *Penjualan, err error) {

	tx := ModelsDb.Table("penjualan").Create(&Penjualan{
		Tanggal:    	u.Tanggal,
		TotalHarga: 	u.TotalHarga,
		TotalBayar: 	u.TotalBayar,
		Kembalian: 		u.Kembalian,
		StatusLunas: 	u.StatusLunas,
	}).Last(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		for _, val := range Detail {
			if u.Id > 0 {
				val["penjualan_id"] = u.Id
				ModelsDb.Table("penjualan_det").Create(val)

				dataStok := map[string]interface{}{
					"keterangan" : "Penjualan " + val["nama"].(string),
					"reff_type" 	: "penjualan_id",
					"reff_id" 	: int32(u.Id),
					"jenis_stok" 	: "keluar",
					"jumlah_keluar" 	: val["jumlah"],
					"harga_keluar" 	: val["harga"],
					"created_at" 	: time.Now().Format("20060102150405"),
				}

				ModelsDb.Table("kartu_stok").Create(dataStok)
			}
		}
	}
	return u, tx.Error
}

// Penjualan Update Function
func (u *Penjualan) Update() (data *Penjualan, err error) {
	tx := ModelsDb.Table("penjualan").Where("id", u.Id).Updates(Penjualan{
		Tanggal:    	u.Tanggal,
		TotalHarga: 	u.TotalHarga,
		TotalBayar: 	u.TotalBayar,
		Kembalian: 		u.Kembalian,
		StatusLunas: 	u.StatusLunas,
	}).First(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		//delete detail
		var query = "DELETE FROM penjualan_det WHERE penjualan_id = " + strconv.Itoa(int(u.Id));
		ModelsDb.Exec(query)
		var queryStok = "DELETE FROM kartu_stok WHERE reff_type = 'penjualan_id' AND reff_id = " + strconv.Itoa(int(u.Id));
		ModelsDb.Exec(queryStok)
		//insert detail
		for _, val := range Detail {
			val["penjualan_id"] = u.Id
			ModelsDb.Table("penjualan_det").Create(val)

			dataStok := map[string]interface{}{
				"keterangan" : "Penjualan " + val["nama"].(string),
				"reff_type" 	: "penjualan_id",
				"reff_id" 		: int32(u.Id),
				"jenis_stok" 	: "keluar",
				"jumlah_keluar" : val["jumlah"],
				"harga_keluar" 	: val["harga"],
				"created_at" 	: time.Now().Format("20060102150405"),
			}

			ModelsDb.Table("kartu_stok").Create(dataStok)
		}
	}
	return u, tx.Error
}

// Penjualan Get Function
func (u *Penjualan) Get() (data *Penjualan, err error) {
	tx := ModelsDb.Table("penjualan").First(&u)
	return u, tx.Error
}

// Penjualan Get Function
func (u *Penjualan) List() (response *qoingrpc.PenjualanList, err error) {
	var data []*qoingrpc.Penjualan
	tx := ModelsDb.Table("penjualan")
	tx.Where("deleted_at IS NULL")
	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.PenjualanList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error

}


func (u *Penjualan) ListDetail() (data []*Penjualan, dataDetail []*PenjualanDetail, err error) {
	tx := ModelsDb.Select("*").Table("penjualan_det").Where("penjualan_id", u.Id).Find(&dataDetail)

	return data, dataDetail, tx.Error
}

// Penjualan Delete Function
func (u *Penjualan) Delete() (data *Penjualan, err error) {
	var queryStok = "DELETE FROM kartu_stok WHERE reff_type = 'penjualan_id' AND reff_id = " + strconv.Itoa(int(u.Id));
	ModelsDb.Exec(queryStok)

	tx := ModelsDb.Table("penjualan").Delete(&u)
	return u, tx.Error
}
