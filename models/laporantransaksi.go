package models

import (
	// "encoding/json"
	"fmt"

	//"go-skeleton-module/global"
	// "go-skeleton-module/rpc/qoingrpc"

	"gorm.io/gorm"
)

type LapFilterTransaksi struct {
	gorm.Model
	UserID    int32
	StartDate string
	EndDate   string
}

type RekMasterTransaksi struct {
	Pemasukan   float32
	Pengeluaran float32
	Saldo       float32
}

type LapDetailTransaksi struct {
	Id            int32
	Tanggal       string
	Keterangan    string
	KategoriId    int32
	Jumlah_masuk  float32
	Jumlah_keluar float32
}

type LapMasterTransaksi struct {
	Tanggal     string
	Pemasukan   float32
	Pengeluaran float32
}

//  931566
// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/
// Dashboard} Get Function
func (u *LapFilterTransaksi) LaporanTransaksi() (data *RekMasterTransaksi, dataDetail []*LapDetailTransaksi, dataDetailTran []*LapMasterTransaksi, err error) {

	tx := ModelsDb.Table("transaksi_keuangan")
	tx.Select("id,DATE(tanggal) as tanggal,keterangan, kategori_id, jumlah_masuk, jumlah_keluar")
	tx.Where("DATE(tanggal) BETWEEN ? AND ?", u.StartDate, u.EndDate)
	tx.Where("user_id = ?", u.UserID)
	tx.Where("deleted_at IS NULL")
	tx.Order("tanggal asc")
	tx.Find(&dataDetail)

	// stringToDate := func(date string) time.Time {
	// 	t, _ := time.Parse("2006-01-02", date)
	// 	return t
	// }

	tx2 := ModelsDb.Table("transaksi_keuangan")
	tx2.Select("sum(jumlah_masuk) as pemasukan, sum(jumlah_keluar) as pengeluaran")
	tx2.Where("DATE(tanggal) BETWEEN ? AND ?", u.StartDate, u.EndDate)
	tx2.Where("user_id = ?", u.UserID)
	tx2.Where("deleted_at IS NULL")
	tx2.Find(&data)

	tx3 := ModelsDb.Table("transaksi_keuangan")
	tx3.Select("DATE(tanggal) as tanggal, sum(jumlah_masuk) as pemasukan, sum(jumlah_keluar) as pengeluaran")
	tx3.Group("tanggal")
	tx3.Where("DATE(tanggal) BETWEEN ? AND ?", u.StartDate, u.EndDate)
	tx3.Where("user_id = ?", u.UserID)
	tx3.Where("deleted_at IS NULL")
	tx3.Find(&dataDetailTran)
	// fmt.Println(u.StartDate, "sapi")
	// for _, val := range dataDetail {
	// 	fmt.Println("---------")
	// 	fmt.Println(*val)
	// }

	// tx := ModelsDb.Table("transaksi_keuangan")
	// tx.Select("DATE(tanggal) as tanggal,keterangan,jumlah_masuk,jumlah_keluar")

	// for _, val := range dataDetailTran {
	// 	a := strings.Split(val.Tanggal, "T")[0]
	// 	test := stringToDate(a)
	// 	y, m, d := test.Date()
	// 	date := strconv.Itoa(y) + "-10-" + strconv.Itoa(d)
	// 	fmt.Println(date, m)
	// 	tx.Order("tanggal asc")
	// 	tx.Where("tanggal = ?", date)
	// }

	//fmt.Println(data, dataDetail, dataDetailTran)

	fmt.Println(tx)
	fmt.Println(tx2)
	fmt.Println(tx3)

	return data, dataDetail, dataDetailTran, tx2.Error
}
