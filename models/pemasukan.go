package models

import (
	"encoding/json"
	"fmt"

	//"go-skeleton-module/global"
	"go-skeleton-module/rpc/qoingrpc"
	// "strconv"

	"gorm.io/gorm"
)

type Pemasukan struct {
	gorm.Model
	Id     int32
	Jumlah float32
	Tanggal string
	Keterangan  string
	KategoriId int32
	Filter string `gorm:"<-:false"`
	Offset int32  `gorm:"<-:false"`
	Limit  int32  `gorm:"<-:false"`
}

type PaginationPemasukan struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Pemasukan) Add() (usr *Pemasukan, err error) {

	tx := ModelsDb.Table("pemasukan").Create(&Pemasukan{
		Jumlah:   float32(u.Jumlah),
		Tanggal: u.Tanggal,
		Keterangan:  u.Keterangan,
		KategoriId: int32(u.KategoriId),
	}).Last(&u)

	fmt.Println("Test")

	return u, tx.Error
}

// Pemasukan Update Function
func (u *Pemasukan) Update() (usr *Pemasukan, err error) {
	tx := ModelsDb.Table("pemasukan").Where("id", u.Id).Updates(Pemasukan{
		Jumlah:   float32(u.Jumlah),
		Tanggal: u.Tanggal,
		Keterangan:  u.Keterangan,
		KategoriId: int32(u.KategoriId),
	}).First(&u)

	return u, tx.Error
}

// Pemasukan Get Function
func (u *Pemasukan) Get() (usr *Pemasukan, err error) {
	tx := ModelsDb.Table("pemasukan").First(&u)
	return u, tx.Error
}

// Pemasukan Get Function
func (u *Pemasukan) List() (response *qoingrpc.PemasukanList, err error) {

	var data []*qoingrpc.Pemasukan
	tx := ModelsDb.Table("pemasukan")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.PemasukanList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error
}

// Pemasukan Delete Function
func (u *Pemasukan) Delete() (usr *Pemasukan, err error) {
	tx := ModelsDb.Table("pemasukan").Delete(&u)
	return u, tx.Error
}
