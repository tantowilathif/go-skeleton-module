package models

import (
	"fmt"
	"go-skeleton-module/rpc/qoingrpc"
	"strconv"
	"strings"
	"time"

	"gorm.io/gorm"
	//p "github.com/Prabandham/paginator"
)

type Dashboard struct {
	gorm.Model
	Id             int32
	UserId         int64
	Tipe           string
	TanggalMulai   string
	TanggalSelesai string
}

type DashboardModel struct {
	gorm.Model
	Label       string
	Pemasukan   string
	Pengeluaran string
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

func Strtotime(date string) time.Time {
	t, _ := time.Parse("2006-01-02", date)
	return t
}

func Timetoreadable(t time.Time) (res string) {
	y, m, d := t.Date()
	res = strconv.Itoa(d) + " " + m.String() + " " + strconv.Itoa(y)
	return
}

func GetDateInterval(tglMulai time.Time, tglSelesai time.Time) []time.Time {
	days := tglSelesai.Sub(tglMulai).Hours() / 24
	interval := int(days)

	arrTgl := []time.Time{tglMulai}

	for i := 0; i < interval; i++ {
		tglPlus1Day := tglMulai.Add(time.Hour * 24)
		arrTgl = append(arrTgl, tglPlus1Day)
		tglMulai = tglPlus1Day
	}

	return arrTgl
}

func Timetoday(t time.Time) (res string) {
	_, _, d := t.Date()
	res = strconv.Itoa(d)
	return res
}

// Dashboard} Get Function
func (u *Dashboard) LapKeuangan() (response *qoingrpc.LaporanPenjualan, err error) {
	var data *qoingrpc.LaporanPenjualan
	tx := ModelsDb.Select("sum(jumlah_masuk) pemasukan, sum(jumlah_keluar) pengeluaran").Table("transaksi_keuangan").Where("DATE(tanggal) BETWEEN ? AND ?", u.TanggalMulai, u.TanggalSelesai).Where("user_id = ?", u.UserId)
	tx.Where("transaksi_keuangan.deleted_at IS NULL")
	tx.Find(&data)

	return &qoingrpc.LaporanPenjualan{
		Pemasukan:   data.Pemasukan,
		Pengeluaran: data.Pengeluaran,
		Total:       data.Pemasukan + data.Pengeluaran,
		Saldo:       data.Pemasukan - data.Pengeluaran,
	}, tx.Error
	//return nil, err
}

// Dashboard Grafik Garis
func (u *Dashboard) DashboardGrafik() (response *qoingrpc.DashboardGrafik, err error) {
	var data []DashboardModel
	tx := ModelsDb.Select("DATE(tanggal) as label, sum(jumlah_masuk) as pemasukan, sum(jumlah_keluar) as pengeluaran").Table("transaksi_keuangan").Group("tanggal").Where("DATE(tanggal) BETWEEN ? AND ?", u.TanggalMulai, u.TanggalSelesai).Where("user_id = ?", u.UserId)
	tx.Where("transaksi_keuangan.deleted_at IS NULL")
	tx.Find(&data)

	tglMulai := Strtotime(u.TanggalMulai)
	tglSelesai := Strtotime(u.TanggalSelesai)
	arrTgl := GetDateInterval(tglMulai, tglSelesai)

	Pemasukan := []float32{}
	Pengeluaran := []float32{}
	Label := []string{}

	var PemasukanWeekCount float32 = 0
	var PengeluaranWeekCount float32 = 0

	for _, val := range arrTgl {

		var pemasukanData float32 = 0
		var pengeluaranData float32 = 0
		tglDariInterval := val.Format("2006-01-02") // Convert tgl yg ada " " nya ke format yyyy-MM-dd

		label := strings.Split(tglDariInterval, "-")[len(strings.Split(tglDariInterval, "-"))-1]

		for _, valDariDb := range data {
			tglDariDb := strings.Split(valDariDb.Label, "T")[0] // Convert tgl yg ada T nya ke format yyyy-MM-dd

			if tglDariDb == tglDariInterval {
				arPemasukan, _ := strconv.ParseFloat(valDariDb.Pemasukan, 32)
				arPengeluaran, _ := strconv.ParseFloat(valDariDb.Pengeluaran, 32)

				pemasukanData = float32(arPemasukan) / 1000
				pengeluaranData = float32(arPengeluaran) / 1000

				break
			}

		}

		Label = append(Label, label)

		// Cek tipe harian / mingguan
		if u.Tipe == "harian" {
			Pemasukan = append(Pemasukan, pemasukanData)
			Pengeluaran = append(Pengeluaran, pengeluaranData)
		} else {
			// Jika mingguan maka tambah data sampai ketemu weekday 1 == senin
			week := int(val.Weekday())
			PemasukanWeekCount += pemasukanData
			PengeluaranWeekCount += pengeluaranData
			if week == 6 {
				Pemasukan = append(Pemasukan, PemasukanWeekCount)
				PemasukanWeekCount = 0
				Pengeluaran = append(Pengeluaran, PengeluaranWeekCount)
				PengeluaranWeekCount = 0
			}
		}
	}

	fmt.Println(PemasukanWeekCount, "masuk")
	fmt.Println(PengeluaranWeekCount, "keluar")
	// Jika ada data sisa maka tambahkan ke array paling belakang -> Khusus mingguan
	if u.Tipe == "mingguan" {
		if PemasukanWeekCount != 0 || PengeluaranWeekCount != 0 {
			Pemasukan = append(Pemasukan, PemasukanWeekCount)
			PemasukanWeekCount = 0

			Pengeluaran = append(Pengeluaran, PengeluaranWeekCount)
			PengeluaranWeekCount = 0
		}

		// Ubah label jika tipe mingguan
		weekLabel := []string{}
		for i := 0; i < len(Pemasukan); i++ {
			weekLabel = append(weekLabel, "Minggu "+strconv.Itoa(i+1))
		}
		Label = weekLabel
	}

	// Hitung total items
	TotalItems := int64(len(Pemasukan))

	return &qoingrpc.DashboardGrafik{
		Tipe:           u.Tipe,
		TanggalMulai:   Timetoreadable(tglMulai),
		TanggalSelesai: Timetoreadable(tglSelesai),
		TotalItems:     TotalItems,
		Pemasukan:      Pemasukan,
		Pengeluaran:    Pengeluaran,
		Label:          Label,
	}, tx.Error
	// return nil, err
}
