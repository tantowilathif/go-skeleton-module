package models

import (
	"gorm.io/gorm"
)

type Auth struct {
	gorm.Model
	Id       int64
	Name     string
	Username string
	Email    string
	Path     string
	Foto     string
	Password string
	Token    string
	URL      string
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Get Function
func (u *Auth) Get() (auth *Auth, err error) {
	tx := ModelsDb.Table("users").Where("username = ? OR email = ?", u.Email, u.Email).Where("password", u.Password).First(&u)
	return u, tx.Error
}

func (u *Auth) TesRasyidGet() (auth *Auth, err error) {
	tx := ModelsDb.Table("users").Where("id = ?", u.Id).First(&u)
	return u, tx.Error
}
