package models

import (
	"go-skeleton-module/rpc/qoingrpc"
	"sort"
	"strconv"
	"strings"

	"gorm.io/gorm"
	//p "github.com/Prabandham/paginator"
)

type TesRasyid struct {
	gorm.Model
	Id             int32
	UserId         int32
	Tipe           string
	TanggalMulai   string
	TanggalSelesai string
}

type TesRasyidModel struct {
	gorm.Model
	Tanggal      string
	Pemasukan    string
	Pengeluaran  string
	KategoriNama string
}

type TesRasyidStok struct {
	gorm.Model
	Id             int32
	UserId         int32
	Keterangan     string
	ReffType       string
	ReffId         int32
	BarangId       int32
	JenisStok      string
	JumlahMasuk    int32
	HargaMasuk     float32
	JumlahKeluar   int32
	HargaKeluar    float32
	CreatedAt      string
	Filter         string `gorm:"<-:false"`
	Offset         int32  `gorm:"<-:false"`
	Limit          int32  `gorm:"<-:false"`
	TanggalMulai   string
	TanggalSelesai string
}

type TesRasyidJumlah struct {
	gorm.Model
	BarangId       string
	TanggalMulai   string
	TanggalSelesai string
	Tipe           string
}

func SplitString(str, splitter string) (res string) {
	res = strings.Split(str, splitter)[0]
	return
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// Tes Detail
func (u *TesRasyid) Detail() (response []*qoingrpc.TesRasyid, err error) {
	var data []TesRasyidModel
	tx := ModelsDb.Select("DATE(tanggal) as tanggal, jumlah_masuk as pemasukan, jumlah_keluar as pengeluaran, kategori.nama as kategori_nama").Joins("join kategori on transaksi_keuangan.kategori_id = kategori.id").Table("transaksi_keuangan").Where("DATE(tanggal) BETWEEN ? AND ?", u.TanggalMulai, u.TanggalSelesai).Where("transaksi_keuangan.user_id = ?", u.UserId).Order("tanggal ASC")
	tx.Find(&data)

	var tes = map[string]*qoingrpc.TesRasyid{}

	for _, valData := range data {
		tgl := strings.Split(valData.Tanggal, "T")[0]
		tes[tgl] = &qoingrpc.TesRasyid{
			Tanggal:          tgl,
			TotalPemasukan:   0,
			TotalPengeluaran: 0,
			Detail:           []*qoingrpc.TesRasyidDetail{},
		}
	}

	for _, valData := range data {
		tgl := strings.Split(valData.Tanggal, "T")[0]
		pemasukan, _ := strconv.ParseInt(valData.Pemasukan, 10, 64)
		pengeluaran, _ := strconv.ParseInt(valData.Pengeluaran, 10, 64)

		tes[tgl] = &qoingrpc.TesRasyid{
			Tanggal:          tgl,
			TotalPemasukan:   tes[tgl].TotalPemasukan + pemasukan,
			TotalPengeluaran: tes[tgl].TotalPengeluaran + pengeluaran,
			Detail: append(tes[tgl].Detail, &qoingrpc.TesRasyidDetail{
				Tanggal:     tgl,
				Pemasukan:   int64(pemasukan),
				Pengeluaran: int64(pengeluaran),
				Kategori:    valData.KategoriNama,
			}),
		}
	}

	var dataRes = []*qoingrpc.TesRasyid{}

	for _, v := range tes {
		dataRes = append(dataRes, v)
	}

	return dataRes, tx.Error
}

// Tes Stok
func (u *TesRasyidStok) TesRasyidStokList() (response *qoingrpc.TesRasyidStokList, err error) {

	var stok []*qoingrpc.TesRasyidStokAwal
	tx := ModelsDb.Select("sum(jumlah_masuk - jumlah_keluar) as stok_awal, barang_id").Table("kartu_stok")
	if u.BarangId != 0 {
		tx.Where("barang_id = ?", u.BarangId)
	}
	if u.TanggalMulai != "" && u.TanggalSelesai != "" {
		tx.Where("DATE(kartu_stok.created_at) < ?", u.TanggalMulai)
	}
	tx.Group("barang_id").Find(&stok)
	var stokMap = make(map[int32]int32, len(stok))
	for _, val := range stok {
		if u.TanggalMulai != "" && u.TanggalSelesai != "" {
			stokMap[val.BarangId] = val.StokAwal
		} else {
			val.StokAwal = 0
		}
	}

	var data []*qoingrpc.TesRasyidStok
	dx := ModelsDb.Select("kartu_stok.*, nama as nama_barang").Table("kartu_stok").Joins("join barang ON kartu_stok.barang_id = barang.id")
	if u.TanggalMulai != "" && u.TanggalSelesai != "" {
		dx.Where("DATE(kartu_stok.created_at) BETWEEN ? AND ?", u.TanggalMulai, u.TanggalSelesai)
	}
	if u.BarangId != 0 {
		dx.Where("barang_id = ?", u.BarangId)
	}
	dx.Find(&data)

	for _, val := range data {
		stokMap[val.BarangId] += val.JumlahMasuk
		stokMap[val.BarangId] -= val.JumlahKeluar
		val.SaldoStok = stokMap[val.BarangId]
	}

	return &qoingrpc.TesRasyidStokList{
		TotalItems: int32(len(data)),
		List:       data,
		DataStok:   stok,
	}, tx.Error

}

// Tes Jmlh
func (u *TesRasyidJumlah) TesRasyidJumlahList() (response *qoingrpc.TesRasyidJumlahData, err error) {
	var data []*qoingrpc.TesRasyidJumlahDb
	tx := ModelsDb.Select("CASE WHEN '"+u.Tipe+"' = 'masuk' THEN SUM(jumlah_masuk) ELSE SUM(jumlah_keluar) END jumlah,"+" DATE(kartu_stok.created_at) created_at, nama nama_barang, barang_id").Table("kartu_stok").Joins("join barang on barang.id = kartu_stok.barang_id").Where("DATE(kartu_stok.created_at) BETWEEN ? AND ?", u.TanggalMulai, u.TanggalSelesai).Where("kartu_stok.deleted_at is NULL").Group("barang_id, created_at")
	tx.Find(&data)

	var hold = make(map[int32]map[string]map[string]interface{})
	var main = []*qoingrpc.TesRasyidJumlahList{}
	var totalBawah = make(map[string]int32)

	tglA := Strtotime(u.TanggalMulai)
	tglB := Strtotime(u.TanggalSelesai)
	allDate := GetDateInterval(tglA, tglB)

	var allDateStr = []string{}
	for _, v := range allDate {
		allDateStr = append(allDateStr, Timetoday(v))
		totalBawah[Timetoday(v)] = 0
	}

	for _, v := range data {
		tgl := Strtotime(SplitString(v.CreatedAt, "T"))
		day := Timetoday(tgl)
		if hold[v.BarangId] == nil {
			hold[v.BarangId] = make(map[string]map[string]interface{}, len(allDateStr))
		}

		for _, d := range allDateStr {
			var inp int32 = 0
			if d == day {
				inp = v.Jumlah
			}

			if hold[v.BarangId][d] == nil {
				hold[v.BarangId][d] = map[string]interface{}{
					"nama_barang": v.NamaBarang,
					"jumlah":      inp,
					"barang_id":   v.BarangId,
				}
			} else {
				hold[v.BarangId][d]["jumlah"] = hold[v.BarangId][d]["jumlah"].(int32) + inp
			}
		}
	}

	for k, v := range hold {
		detail := []int32{}

		// Sort map by key
		keys := make([]string, 0, len(v))
		for k, val := range v {
			totalBawah[k] += val["jumlah"].(int32)
			keys = append(keys, k)
		}
		sort.Strings(keys)

		count := int32(0)
		for _, k := range keys {
			count += v[k]["jumlah"].(int32)
			detail = append(detail, v[k]["jumlah"].(int32))
		}
		detail = append(detail, count)

		data := &qoingrpc.TesRasyidJumlahList{
			BarangId:   k,
			NamaBarang: v[allDateStr[0]]["nama_barang"].(string),
			Detail:     detail,
		}

		main = append(main, data)
	}

	// order map totalBawah by key
	keys := make([]string, 0, len(totalBawah))
	for k, _ := range totalBawah {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var total = []int32{}
	inputAll := int32(0)
	for _, k := range keys {
		total = append(total, totalBawah[k])
		inputAll += totalBawah[k]
	}
	total = append(total, inputAll)

	return &qoingrpc.TesRasyidJumlahData{
		Tanggal: allDateStr,
		List:    main,
		Total:   total,
	}, tx.Error

}
