package models

import (
	"encoding/json"
	"fmt"

	//"go-skeleton-module/global"
	"go-skeleton-module/rpc/qoingrpc"
	"strconv"

	"gorm.io/gorm"
)

type Customer struct {
	gorm.Model
	Id     int32
	Nama   string
	NoTelp int64
	Email  string
	Alamat string
	Detail string `gorm:"<-:false"`
	Filter string `gorm:"<-:false"`
	Offset int32  `gorm:"<-:false"`
	Limit  int32  `gorm:"<-:false"`
}

type CustomerDetail struct {
	Id            int32
	CustomerId    int32
	ContactPerson int32
	NoTelp        int64
	Bank          string
	NamaRekening  string
	NomerRekening int32
}

type PaginationCustomer struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Customer) Add() (usr *Customer, err error) {

	tx := ModelsDb.Table("customer").Create(&Customer{
		Nama:   u.Nama,
		NoTelp: u.NoTelp,
		Email:  u.Email,
		Alamat: u.Alamat,
	}).Last(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		for _, val := range Detail {
			if u.Id > 0 {
				val["customer_id"] = u.Id
				ModelsDb.Table("customer_det").Create(val)
			}
		}
	}

	return u, tx.Error
}

// Customer Update Function
func (u *Customer) Update() (usr *Customer, err error) {
	tx := ModelsDb.Table("customer").Where("id", u.Id).Updates(Customer{
		Nama:   u.Nama,
		NoTelp: u.NoTelp,
		Email:  u.Email,
		Alamat: u.Alamat,
	}).First(&u)

	var Detail []map[string]interface{}
	json.Unmarshal([]byte(u.Detail), &Detail)

	if Detail != nil {
		//delete detail
		var query = "DELETE FROM customer_det WHERE customer_id = " + strconv.Itoa(int(u.Id))
		ModelsDb.Exec(query)

		//insert detail
		for _, val := range Detail {
			val["customer_id"] = u.Id
			ModelsDb.Table("customer_det").Create(val)
		}
	}

	return u, tx.Error
}

// Customer Get Function
func (u *Customer) Get() (usr *Customer, err error) {
	tx := ModelsDb.Table("customer").First(&u)
	return u, tx.Error
}

// Customer Get Function
func (u *Customer) List() (response *qoingrpc.CustomerList, err error) {

	var data []*qoingrpc.Customer
	tx := ModelsDb.Table("customer")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.CustomerList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error
}

func (u *Customer) ListDetail() (data []*Customer, dataDetail []*CustomerDetail, err error) {
	tx := ModelsDb.Select("*").Table("customer_det").Where("customer_id", u.Id).Find(&dataDetail)

	return data, dataDetail, tx.Error
}

// Customer Delete Function
func (u *Customer) Delete() (usr *Customer, err error) {
	tx := ModelsDb.Table("customer").Delete(&u)
	return u, tx.Error
}
