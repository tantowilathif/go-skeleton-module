package models

import (
	"encoding/json"
	"fmt"

	//"go-skeleton-module/global"
	"go-skeleton-module/rpc/qoingrpc"
	// "strconv"

	"gorm.io/gorm"
)

type Pengeluaran struct {
	gorm.Model
	Id     int32
	Jumlah float32
	Tanggal string
	Keterangan  string
	KategoriId int32
	Filter string `gorm:"<-:false"`
	Offset int32  `gorm:"<-:false"`
	Limit  int32  `gorm:"<-:false"`
}

type PaginationPengeluaran struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&User{}).argument(...)
*/

// User Add Function
func (u *Pengeluaran) Add() (usr *Pengeluaran, err error) {

	tx := ModelsDb.Table("pengeluaran").Create(&Pengeluaran{
		Jumlah:   float32(u.Jumlah),
		Tanggal: u.Tanggal,
		Keterangan:  u.Keterangan,
		KategoriId: int32(u.KategoriId),
	}).Last(&u)

	return u, tx.Error
}

// Pengeluaran Update Function
func (u *Pengeluaran) Update() (usr *Pengeluaran, err error) {
	tx := ModelsDb.Table("pengeluaran").Where("id", u.Id).Updates(Pengeluaran{
		Jumlah:   float32(u.Jumlah),
		Tanggal: u.Tanggal,
		Keterangan:  u.Keterangan,
		KategoriId: int32(u.KategoriId),
	}).First(&u)

	return u, tx.Error
}

// Pengeluaran Get Function
func (u *Pengeluaran) Get() (usr *Pengeluaran, err error) {
	tx := ModelsDb.Table("pengeluaran").First(&u)
	return u, tx.Error
}

// Pengeluaran Get Function
func (u *Pengeluaran) List() (response *qoingrpc.PengeluaranList, err error) {

	var data []*qoingrpc.Pengeluaran
	tx := ModelsDb.Table("pengeluaran")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.PengeluaranList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error
}

// Pengeluaran Delete Function
func (u *Pengeluaran) Delete() (usr *Pengeluaran, err error) {
	tx := ModelsDb.Table("pengeluaran").Delete(&u)
	return u, tx.Error
}
