package models

import (
	"encoding/json"
	"fmt"
	"go-skeleton-module/rpc/qoingrpc"

	"gorm.io/gorm"
)

type Bantuan struct {
	gorm.Model
	Id          int64
	Judul       string
	Keterangan  string
	LinkV       string
	LinkYoutube string
	Filter      string `gorm:"<-:false"`
	Offset      int32  `gorm:"<-:false"`
	Limit       int32  `gorm:"<-:false"`
}

type PaginationBantuan struct {
	Limit  int
	Offset int
	Count  int
}

// Base on Table
/*
	Example : ModelsDB.Table("tableNames").argument(...)
*/

// Base on Struct
/*
	Example : ModelsDb.Model(&Bantuan{}).argument(...)
*/

// Bantuan Add Function
func (u *Bantuan) Add() (usr *Bantuan, err error) {
	tx := ModelsDb.Table("bantuan").Create(&Bantuan{
		Judul:       u.Judul,
		Keterangan:  u.Keterangan,
		LinkV:       u.LinkV,
		LinkYoutube: u.LinkYoutube,
	}).Last(&u)
	return u, tx.Error
}

// Bantuan Update Function
func (u *Bantuan) Update() (usr *Bantuan, err error) {
	tx := ModelsDb.Table("bantuan").Where("id", u.Id).Updates(Bantuan{
		Judul:       u.Judul,
		Keterangan:  u.Keterangan,
		LinkV:       u.LinkV,
		LinkYoutube: u.LinkYoutube,
	}).First(&u)

	return u, tx.Error
}

// Bantuan Get Function
func (u *Bantuan) Get() (usr *Bantuan, err error) {
	tx := ModelsDb.Table("bantuan").First(&u)
	return u, tx.Error
}

// Bantuan Get Function
func (u *Bantuan) List() (response *qoingrpc.BantuanList, err error) {

	var data []*qoingrpc.Bantuan
	tx := ModelsDb.Table("bantuan")

	var filter map[string]interface{}
	json.Unmarshal([]byte(u.Filter), &filter)
	tx.Where("deleted_at IS NULL")
	//FILTER
	for index, element := range filter {
		value := fmt.Sprintf("%v", element)
		tx.Where(index+" LIKE ?", "%"+value+"%")
	}

	var count int64
	tx.Count(&count)

	//PAGINATION
	if u.Limit > 0 {
		tx.Limit(int(u.Limit))
	}
	if u.Offset > 0 {
		tx.Offset(int(u.Offset))
	}
	tx.Order("id DESC")
	tx.Find(&data)

	return &qoingrpc.BantuanList{
		Limit:      u.Limit,
		Offset:     u.Offset,
		TotalItems: int32(count),
		List:       data,
	}, tx.Error

}

// Bantuan Delete Function
func (u *Bantuan) Delete() (usr *Bantuan, err error) {
	tx := ModelsDb.Table("bantuan").Delete(&u)
	return u, tx.Error
}
