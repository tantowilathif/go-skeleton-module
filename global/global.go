package global

import (
	"crypto/md5"
	"encoding/hex"
	"go-skeleton-module/models"

	"gorm.io/gorm"
)

type SisaStok struct {
	gorm.Model
	Barang_Id     int32
	Masuk         int32
	Jenis_Stok    string
	Jumlah_Masuk  int32
	Jumlah_Keluar int32
}

func (param *SisaStok) GetSisaStok() (response int32, err error) {
	kstok := models.ModelsDb.Select("SUM(jumlah_masuk) as jumlah_masuk,SUM(jumlah_keluar) as jumlah_keluar").Table("kartu_stok")
	kstok.Where("barang_id", param.Barang_Id).Find(&param)
	response = param.Jumlah_Masuk - param.Jumlah_Keluar
	return response, err
}
func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}
